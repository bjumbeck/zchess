// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "StandardChessGameMode.generated.h"

UCLASS()
class ZCHESS_API AStandardChessGameMode : public AGameMode
{
	GENERATED_BODY()
	
    public:
        AStandardChessGameMode(const FObjectInitializer& ObjectInitializer);
};
