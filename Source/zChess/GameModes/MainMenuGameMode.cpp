// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "MainMenuGameMode.h"
#include "Controllers/ChessPlayerController.h"
#include "Online/ChessGameSession.h"

AMainMenuGameMode::AMainMenuGameMode(const FObjectInitializer& objectInitializer)
    : Super(objectInitializer)
{
    DefaultPawnClass = NULL;
    PlayerControllerClass = AChessPlayerController::StaticClass();
    
    bUseSeamlessTravel = true;
}

TSubclassOf<AGameSession> AMainMenuGameMode::GetGameSessionClass() const
{
    return AChessGameSession::StaticClass();
}