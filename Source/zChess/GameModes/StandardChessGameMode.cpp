// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "StandardChessGameMode.h"

AStandardChessGameMode::AStandardChessGameMode(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    // Default Classes
    DefaultPawnClass = NULL;
    // PlayerControllerClass = AzChessPlayerController::StaticClass();
}
