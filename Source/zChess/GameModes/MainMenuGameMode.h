// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ZCHESS_API AMainMenuGameMode : public AGameMode
{
	GENERATED_BODY()

    public:
        AMainMenuGameMode(const FObjectInitializer& objectInitializer);


    protected:
        virtual TSubclassOf<AGameSession> GetGameSessionClass() const override;
};
