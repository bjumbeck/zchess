// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/BoxComponent.h"
#include "ChessTileComponent.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent), ClassGroup = (ChessboardComponents))
class ZCHESS_API UChessTileComponent : public UBoxComponent
{
	GENERATED_BODY()
	
    public:
        UChessTileComponent(const FObjectInitializer& ObjectInitializer);

        FString getTilePositionString() const { return tilePositionString; }
        void setTilePositionString(const FString& value) { tilePositionString = value; }

        UFUNCTION()
        void OnTileOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

        UFUNCTION()
        void OnTileOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

    private:
        FString tilePositionString;
        bool occupied;
};
