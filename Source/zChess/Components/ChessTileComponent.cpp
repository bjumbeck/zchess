// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessTileComponent.h"

UChessTileComponent::UChessTileComponent(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    tilePositionString = FString(TEXT("**"));
    occupied = false;

    BoxExtent = FVector(1.35f, 1.35f, 1.35f);
    RelativeLocation.Z = 1.4f;

    // Overlap events and collision setup
    //this->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics); // Do we need?
    this->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));

    this->OnComponentBeginOverlap.AddDynamic(this, &UChessTileComponent::OnTileOverlapBegin);
    this->OnComponentEndOverlap.AddDynamic(this, &UChessTileComponent::OnTileOverlapEnd);
}

void UChessTileComponent::OnTileOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    occupied = true;
}

void UChessTileComponent::OnTileOverlapEnd(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    occupied = false;
}