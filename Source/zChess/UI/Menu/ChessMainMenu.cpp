// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessMainMenu.h"
#include "ChessGameInstance.h"
#include "UI/Styles/ChessMenuSoundsWidgetStyle.h"
#include "UI/Styles/ChessStyle.h"
#include "UI/Menu/Widgets/SChessMenuItem.h"
#include "Player/ChessLocalPlayer.h"

#define LOCTEXT_NAMESPACE "ChessGame.HUD.Menu"

static const FString TimeLimits[] = { TEXT("15 Minutes"), TEXT("30 Minutes"), TEXT("UNLIMTED") };
static const FString MapNames[] = { TEXT("ChessLevel"), TEXT("UnrankedChessLevel"), TEXT("RankedChessLevel") };
static const FString JoinMapNames[] = { TEXT("Any"), TEXT("ChessLevel"), TEXT("UnrankedChessLevel"), TEXT("RankedChessLevel") };
static const FString PackageNames[] = { TEXT("ChessLevel.umap") };
static const int32 DefaultRankedMap = 0;
static const int32 DefaultUnrankedMap = 0;

FChessMainMenu::~FChessMainMenu()
{
    auto sessions = Online::GetSessionInterface();
}

void FChessMainMenu::Construct(TWeakObjectPtr<UChessGameInstance> _GameInstance, TWeakObjectPtr<ULocalPlayer> _PlayerOwner)
{
    playerOwner = _PlayerOwner;
    matchType = EMatchType::Custom;

    check(_GameInstance.IsValid());
    auto sessions = Online::GetSessionInterface();
    gameInstance = _GameInstance;

    isLanGame = false;

    // Time limits list
    TArray<FText> timeLimitsList;
    for (int32 i = 0; i < ARRAY_COUNT(TimeLimits); ++i)
    {
        timeLimitsList.Add(FText::FromString(TimeLimits[i]));
    }

    // List of Map Names
    TArray<FText> mapList;
    for (int32 i = 0; i < ARRAY_COUNT(MapNames); ++i)
    {
        mapList.Add(FText::FromString(MapNames[i]));
    }

    // List of Map Names we can join
    TArray<FText> joinMapList;
    for (int32 i = 0; i < ARRAY_COUNT(JoinMapNames); ++i)
    {
        joinMapList.Add(FText::FromString(JoinMapNames[i]));
    }

    // On/Off Text List
    TArray<FText> onOffList;
    onOffList.Add(LOCTEXT("Off", "OFF"));
    onOffList.Add(LOCTEXT("On", "ON"));

    // Now we can build the menu
    menuWidget.Reset();
    menuWidgetContainer.Reset();

    if (GEngine && GEngine->GameViewport)
    {
        SAssignNew(menuWidget, SChessMenuWidget)
            .Cursor(EMouseCursor::Default)
            .playerOwner(GetPlayerOwner())
            .isInGameMenu(false);

        SAssignNew(menuWidgetContainer, SWeakWidget)
            .PossiblyNullContent(menuWidget);

        TSharedPtr<FChessMenuItem> rootMenuItem;
        TSharedPtr<FChessMenuItem> menuItem;

        // Host Game Menu Item
        menuItem = MenuHelper::AddMenuItem(rootMenuItem, LOCTEXT("Host", "HOST GAME"));
        // Sub menu for "Host Game"
        MenuHelper::AddMenuItemSP(menuItem, LOCTEXT("HostUnranked", "HOST UNRANKED GAME"), this, &FChessMainMenu::OnUIHostUnrankedGame);
        MenuHelper::AddMenuItemSP(menuItem, LOCTEXT("HostRanked", "HOST RANKED GAME"), this, &FChessMainMenu::OnUIHostRankedGame);
        timeLimitOption = MenuHelper::AddMenuOptionSP(menuItem, LOCTEXT("TimeLimit", "Time Limit"), timeLimitsList, this, &FChessMainMenu::OnTimeLimitOptionChanged);
        hostLANItem = MenuHelper::AddMenuOptionSP(menuItem, LOCTEXT("LanMatch", "LAN"), onOffList, this, &FChessMainMenu::OnLANMatchOptionChanged);
        hostLANItem->SelectedMultiChoiceIndex = isLanGame;

        // Join Game Menu Item
        menuItem = MenuHelper::AddMenuItem(rootMenuItem, LOCTEXT("Join", "JOIN GAME"));
        // Sub menu for "Join Game"
		// Online Game Lobby List Widget (Called when we execute join game)
		MenuHelper::AddCustomMenuItem(joinGameLobbyItem, SAssignNew(gameLobbyWidget, SChessGameLobby).ownerWidget(menuWidget).playerOwner(GetPlayerOwner()));
		MenuHelper::AddMenuItemSP(menuItem, LOCTEXT("OnlineGameLobyy", "Game Lobby"), this, &FChessMainMenu::OnJoinServer);
        joinLANItem = MenuHelper::AddMenuOptionSP(menuItem, LOCTEXT("LanMatch", "LAN"), onOffList, this, &FChessMainMenu::OnLANMatchOptionChanged);
        joinLANItem->SelectedMultiChoiceIndex = isLanGame;

        // Quit Item
        MenuHelper::AddMenuItemSP(rootMenuItem, LOCTEXT("Quit", "QUIT"), this, &FChessMainMenu::OnUIQuit);

        // Finish up the Menu
        menuWidget->CurrentMenuTitle = LOCTEXT("MainMenuTitle", "MAIN MENU");
        menuWidget->OnMenuGoBack.BindSP(this, &FChessMainMenu::OnMenuGoBack);
        menuWidget->MainMenu = menuWidget->CurrentMenu = rootMenuItem->SubMenu;
        menuWidget->OnMenuHidden.BindSP(this, &FChessMainMenu::OnMenuHidden);

        menuWidget->BuildAndShowMenu();
    }
}

void FChessMainMenu::AddMenuToGameViewport()
{
    if (GEngine && GEngine->GameViewport)
    {
        GEngine->GameViewport->AddViewportWidgetContent(menuWidgetContainer.ToSharedRef());
    }
}

void FChessMainMenu::RemoveMenuFromGameViewport()
{
    if (GEngine && GEngine->GameViewport)
    {
        GEngine->GameViewport->RemoveViewportWidgetContent(menuWidgetContainer.ToSharedRef());
    }
}

ULocalPlayer* FChessMainMenu::GetPlayerOwner() const
{
    return playerOwner.Get();
}

int32 FChessMainMenu::GetPlayerOwnerControllerId() const
{
    return playerOwner.IsValid() ? playerOwner->GetControllerId() : -1;
}

FString FChessMainMenu::GetMapName() const
{
    return MapNames[StaticCast<int>(GetSelectedMap())];
}

FChessMainMenu::EMap FChessMainMenu::GetSelectedMap() const
{
    if (gameInstance.IsValid() && hostOnlineMapOption.IsValid())
    {
        return StaticCast<EMap>(hostOnlineMapOption->SelectedMultiChoiceIndex);
    }

    return EMap::EOfflineGame; // TODO: We really don't need maps at the moment but can see the need arising later so including this functionality for now. Change it later.
}

void FChessMainMenu::CloseSubMenu()
{
    menuWidget->MenuGoBack(true);
}

void FChessMainMenu::OnMenuGoBack(MenuPtrType menu)
{
    // This is included just in case you need to do something when moving out a menu.
    // These could include stuff like reverting changing if not saved on a options menu or something
}

void FChessMainMenu::OnMenuHidden()
{
    RemoveMenuFromGameViewport();
}

void FChessMainMenu::OnHostOnlineSelected()
{
    StartOnlinePrivilegeTask(IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate::CreateSP(this, &FChessMainMenu::OnUserCanPlayOnlineHost));
}

void FChessMainMenu::OnHostOfflineSelected()
{
    matchType = EMatchType::Custom;

    // TODO: We need to re implement this for LAN handling
//     if (gameInstance.IsValid())
//     {
//         gameInstance->SetIsOnline(false);
//     }

    menuWidget->EnterSubMenu();
}

void FChessMainMenu::OnLANMatchOptionChanged(TSharedPtr<FChessMenuItem> menuItem, int32 multiOptionIndex)
{
    if (hostLANItem.IsValid())
    {
        hostLANItem->SelectedMultiChoiceIndex = multiOptionIndex;
    }

    check(joinLANItem.IsValid());
    joinLANItem->SelectedMultiChoiceIndex = multiOptionIndex;
    isLanGame = multiOptionIndex > 0;
}

void FChessMainMenu::OnTimeLimitOptionChanged(TSharedPtr<FChessMenuItem> menuItem, int32 multiOptionIndex)
{
    if (timeLimitOption.IsValid())
    {
        timeLimitOption->SelectedMultiChoiceIndex = multiOptionIndex;
    }

    // TODO: Set whatever we need to, so that we can pass in the time limit to the game mode later
}

void FChessMainMenu::OnUIHostUnrankedGame()
{
    #if WITH_EDITOR
    if (GIsEditor)
    {
        return;
    }
    #endif

    menuWidget->LockControls(true);
    menuWidget->HideMenu();

    UWorld* const world = gameInstance.IsValid() ? gameInstance->GetWorld() : nullptr;
    if (world && GetPlayerOwnerControllerId() != -1)
    {
        const FChessMenuSoundsStyle& menuSounds = FChessStyle::Get().GetWidgetStyle<FChessMenuSoundsStyle>("DefaultChessMenuSoundsStyle");
        MenuHelper::PlaySoundAndCall(world, menuSounds.StartGameSound, GetPlayerOwnerControllerId(), this, &FChessMainMenu::HostUnrankedGame);
    }
}

void FChessMainMenu::OnUIHostRankedGame()
{
    #if WITH_EDITOR
    if (GIsEditor)
    {
        return;
    }
    #endif

    menuWidget->LockControls(true);
    menuWidget->HideMenu();

    UWorld* const world = gameInstance.IsValid() ? gameInstance->GetWorld() : nullptr;
    if (world && GetPlayerOwnerControllerId() != -1)
    {
        const FChessMenuSoundsStyle& menuSounds = FChessStyle::Get().GetWidgetStyle<FChessMenuSoundsStyle>("DefaultChessMenuSoundsStyle");
        MenuHelper::PlaySoundAndCall(world, menuSounds.StartGameSound, GetPlayerOwnerControllerId(), this, &FChessMainMenu::HostRankedGame);
    }
}

void FChessMainMenu::HostGame(const FString& gameType)
{
    if (ensure(gameInstance.IsValid()) && GetPlayerOwner() != NULL)
    {
        const FString startURL = FString::Printf(TEXT("/Game/Maps/%s?game=%s%s%s%s"), *GetMapName(), *gameType, TEXT("?listen"), isLanGame ? TEXT("?bIsLanMatch") : TEXT(""), TEXT(""));
        gameInstance->HostGame(GetPlayerOwner(), gameType, startURL);
    }
}

void FChessMainMenu::HostUnrankedGame()
{
    HostGame(TEXT("UnrankedGame"));
}

void FChessMainMenu::HostRankedGame()
{
    HostGame(TEXT("RankedGame"));
}

void FChessMainMenu::OnJoinServer()
{
    StartOnlinePrivilegeTask(IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate::CreateSP(this, &FChessMainMenu::OnUserCanPlayOnlineJoin));
}

void FChessMainMenu::OnUIQuit()
{
    LockAndHideMenu();

    const FChessMenuSoundsStyle& menuSounds = FChessStyle::Get().GetWidgetStyle<FChessMenuSoundsStyle>("DefaultChessMenuSoundsStyle");
    UWorld* const world = gameInstance.IsValid() ? gameInstance->GetWorld() : nullptr;
    if (world && GetPlayerOwnerControllerId() != -1)
    {
        MenuHelper::PlaySoundAndCall(world, menuSounds.ExitGameSound, GetPlayerOwnerControllerId(), this, &FChessMainMenu::Quit);
    }
}

void FChessMainMenu::Quit()
{
    if (ensure(gameInstance.IsValid()))
    {
        UGameViewportClient* const viewport = gameInstance->GetGameViewportClient();
        if (ensure(viewport))
        {
            viewport->ConsoleCommand("quit");
        }
    }
}

void FChessMainMenu::LockAndHideMenu()
{
    menuWidget->LockControls(true);
    menuWidget->HideMenu();
}

UChessPersistentUser* FChessMainMenu::GetPersistentUser() const
{
    UChessLocalPlayer* const localPlayer = Cast<UChessLocalPlayer>(GetPlayerOwner());
    return localPlayer ? localPlayer->GetPersistentUser() : nullptr;
}

void FChessMainMenu::StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& delegate)
{
    if (gameInstance.IsValid())
    {
        // Lock controls for the duration of the async task
        menuWidget->LockControls(true);
        TSharedPtr<FUniqueNetId> userId;
        if (playerOwner.IsValid())
        {
            userId = playerOwner->GetPreferredUniqueNetId();
        }

        gameInstance->StartOnlinePrivilegeTask(delegate, EUserPrivileges::CanPlayOnline, userId);
    }
}

void FChessMainMenu::CleanupOnlinePrivilegeTask()
{
    if (gameInstance.IsValid())
    {
        gameInstance->CleanupOnlinePrivilegeTask();
    }
}

void FChessMainMenu::OnUserCanPlayOnlineHost(const FUniqueNetId& userId, EUserPrivileges::Type privilege, uint32 privilegeResults)
{
    CleanupOnlinePrivilegeTask();
    menuWidget->LockControls(false);

    if (privilegeResults == StaticCast<uint32>(IOnlineIdentity::EPrivilegeResults::NoFailures))
    {
        matchType = EMatchType::Custom;

//         if (GameInstance.IsValid())
//         {
//             GameInstance->SetIsOnline(true);
//         }
        
        menuWidget->EnterSubMenu();
    }
    else if (gameInstance.IsValid())
    {
        // Display failure dialogs (TODO)
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Failed on authorization of online privileges"));
    }
}

void FChessMainMenu::OnUserCanPlayOnlineJoin(const FUniqueNetId& userId, EUserPrivileges::Type privilege, uint32 privilegeResults)
{
    CleanupOnlinePrivilegeTask();
    menuWidget->LockControls(false);

    if (privilegeResults == StaticCast<uint32>(IOnlineIdentity::EPrivilegeResults::NoFailures))
    {   
        matchType = EMatchType::Custom;

        //         if (GameInstance.IsValid())
        //         {
        //             GameInstance->SetIsOnline(true);
        //         }

        menuWidget->NextMenu = joinGameLobbyItem->SubMenu;
        gameLobbyWidget->BeginServerSearch(isLanGame, TEXT("Any")); // We can use the map filter for different game modes (Or is this a bad idea?)
        gameLobbyWidget->UpdateOnlineGameList();
        menuWidget->EnterSubMenu();
    }
    else if (gameInstance.IsValid())
    {
        // Display failure dialogs (TODO)
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Failed on authorization of online privileges"));
    }
}

#undef LOCTEXT_NAMESPACE