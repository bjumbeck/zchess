// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"
#include "Widgets/ChessMenuItem.h"
#include "Widgets/SChessMenuItem.h"
#include "Widgets/SChessGameLobby.h"

class ZCHESS_API FChessMainMenu : public TSharedFromThis<FChessMainMenu>
{
    protected:
        enum class EMap
        {
            EOfflineGame,
            EOnlineGame,
            EOnlineRankedGame,
        };

        enum class EMatchType
        {
            Custom,
            Unranked,
            Ranked,
        };

    public:
	    ~FChessMainMenu();

        /// Builds the menu
        void Construct(TWeakObjectPtr<class UChessGameInstance> _GameInstance, TWeakObjectPtr<class ULocalPlayer> _PlayerOwner);

        /// Add the menu to the game view port so it becomes visible
        void AddMenuToGameViewport();

        /// Remove the menu from the game view port
        void RemoveMenuFromGameViewport();

        /// Returns the player that owns the main menu
        class ULocalPlayer* GetPlayerOwner() const;

        /// Returns the controller id for the player that owns the menu
        int32 GetPlayerOwnerControllerId() const;

        /// Returns the string name of the currently selected map
        FString GetMapName() const;

    protected:
        EMap GetSelectedMap() const;

        /// Closes the sub menu (Goes back in the menu structure)
        void CloseSubMenu();

        /// Called when going back to the previous menu
        void OnMenuGoBack(MenuPtrType menu);

        /// Called when the menu hide animation is finished
        void OnMenuHidden();

        /// Called when the user selects "HOST ONLINE"
        void OnHostOnlineSelected();

        /// Called when the user selects "HOST OFFLINE"
        void OnHostOfflineSelected();

        /// LAN Match option changed callback
        void OnLANMatchOptionChanged(TSharedPtr<FChessMenuItem> menuItem, int32 multiOptionIndex);

        /// Time limit option changed callback
        void OnTimeLimitOptionChanged(TSharedPtr<FChessMenuItem> menuItem, int32 multiOptionIndex);

        /// Plays the StartGameSound and calls HostUnrankedGame after the sound is played
        void OnUIHostUnrankedGame();

        /// Plays the StartGameSound and calls HostRankedGame after the sound is played
        void OnUIHostRankedGame();

        /// Hosts a game using the passed in game type
        void HostGame(const FString& gameType);

        /// Hosts a unranked game
        void HostUnrankedGame();

        /// Hosts a ranked game
        void HostRankedGame();

        /// Join online game
        void OnJoinServer();

        /// Plays sound and calls quit
        void OnUIQuit();

        /// Quits the game
        void Quit();

        /// Lock the controls and hide the main menu
        void LockAndHideMenu();

        /// Callback for when game is created
        // void OnGameCreated(bool wasSuccessful);

        class UChessPersistentUser* GetPersistentUser() const;

        void StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& delegate);

        void CleanupOnlinePrivilegeTask();

        void OnUserCanPlayOnlineHost(const FUniqueNetId& userId, EUserPrivileges::Type privilege, uint32 privilegeResults);
        void OnUserCanPlayOnlineJoin(const FUniqueNetId& userId, EUserPrivileges::Type privilege, uint32 privilegeResults);

    protected:
        /// Owning Game Instance
        TWeakObjectPtr<class UChessGameInstance> gameInstance;

        /// Owning Player
        TWeakObjectPtr<class ULocalPlayer> playerOwner;

        /// Menu Widget
        TSharedPtr<class SChessMenuWidget> menuWidget;

        /// Used for removing the menuWidget
        TSharedPtr<class SWeakWidget> menuWidgetContainer;

        /// The online game lobby list widget
        TSharedPtr<class SChessGameLobby> gameLobbyWidget;

        /// Custom menu for the join game lobby
        TSharedPtr<class FChessMenuItem> joinGameLobbyItem;

        /// LAN Options
        TSharedPtr<class FChessMenuItem> hostLANItem;
        TSharedPtr<class FChessMenuItem> joinLANItem;

        // Time limit option
        TSharedPtr<class FChessMenuItem> timeLimitOption;

        /// Map Selection Widget
        TSharedPtr<FChessMenuItem> hostOfflineMapOption;
        TSharedPtr<FChessMenuItem> hostOnlineMapOption;
        TSharedPtr<FChessMenuItem> joinMapOption;

        /// Match Type
        EMatchType matchType;

        /// LAN Game?
        bool isLanGame;
};
