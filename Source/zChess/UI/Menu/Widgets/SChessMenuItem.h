// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"


class ZCHESS_API SChessMenuItem : public SCompoundWidget
{
    public:
        // Used to declare a delegate type for when the Arrow button is clicked
        DECLARE_DELEGATE_OneParam(FOnArrowPressed, int);

	    SLATE_BEGIN_ARGS(SChessMenuItem)
	    {}

        SLATE_ARGUMENT(TWeakObjectPtr<class ULocalPlayer>, playerOwner)
        SLATE_ARGUMENT(bool, isMultiChoiceMenuItem)
        SLATE_ARGUMENT(TOptional<float>, inactiveTextAlpha)

        SLATE_EVENT(FOnClicked, onClicked)
        SLATE_EVENT(FOnArrowPressed, onArrowPressed)

        SLATE_ATTRIBUTE(FString, text)
        SLATE_ATTRIBUTE(FString, optionText)

	    SLATE_END_ARGS()

	    /** Constructs this widget with InArgs */
	    void Construct(const FArguments& InArgs);

        virtual bool SupportsKeyboardFocus() const override { return true; }

        virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;
        virtual FReply OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

        void SetMenuItemActive(bool isMenuItemActive);
        void UpdateItemText(const FString& updatedText);

    private:
        FSlateColor GetButtonBackgroundColor() const;
        FSlateColor GetButtonTextColor() const;
        FLinearColor GetButtonTextShadowColor() const;

        EVisibility GetLeftArrowVisibility() const;
        EVisibility GetRightArrowVisibility() const;

        FMargin GetOptionPadding() const;
        
        FReply OnRightArrowDown(const FGeometry& myGeometry, const FPointerEvent& mouseEvent);
        FReply OnLeftArrowDown(const FGeometry& myGeometry, const FPointerEvent& mouseEvent);

    public:
        EVisibility LeftArrowVisible;
        EVisibility RightArrowVisible;

    protected:
        /// The delegate to execute when the button is clicked
        FOnClicked onClicked;

        /// The delegate to execute when one of the arrow buttons is pressed
        FOnArrowPressed onArrowPressed;

    private:
        /// The menu item's text attribute
        TAttribute<FString> text;

        /// The menu item's option text attribute
        TAttribute<FString> optionText;

        /// The menu item's text widget
        TSharedPtr<STextBlock> textWidget;

        /// The menu item's text color
        FLinearColor textColor;

        /// The menu item's margin
        float itemMargin;

        /// The menu item's text alpha when it is inactive
        float inactiveTextAlpha;

        /// Is this menu item active?
        bool isActiveMenuItem;

        /// Is this menu item a multiple choice item?
        bool isMultiChoiceMenuItem;

        /// The pointer to our parent PC
        TWeakObjectPtr<class ULocalPlayer> playerOwner;

        /// The style for the menu item
        const struct FChessMenuItemStyle* menuItemStyle;
};
