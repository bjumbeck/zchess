// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"
#include "SChessMenuItem.h"

namespace EChessMenuItemType
{
    enum Type
    {
        Root,
        Standard,
        MultiChoice,
        CustomWidget,
    };
};

typedef TArray<TSharedPtr<class FChessMenuItem>> MenuPtrType;

class FChessMenuInfo
{
    public:
        FChessMenuInfo(MenuPtrType menu, int32 selectedIndex, FText menuTitle)
            : Menu(menu)
            , SelectedIndex(selectedIndex)
            , MenuTitle(menuTitle)
        {
        }

        /// The menu, which is just an array composed of FChessMenuItem objects
        MenuPtrType Menu;

        /// Current selection for this menu
        int32 SelectedIndex;

        /// The title for this menu
        FText MenuTitle;
};

// The actual class for MenuItem's. It is the wrapper for the underlying slate class
class ZCHESS_API FChessMenuItem : public TSharedFromThis<FChessMenuItem>
{
    public:
        // Delegate types
        DECLARE_DELEGATE(FOnConfirmMenuItem);
        DECLARE_DELEGATE_TwoParams(FOnOptionChanged, TSharedPtr<FChessMenuItem>, int32);

    public:
        /// Constructor for standard menu items
        FChessMenuItem(FText itemText)
            : IsVisible(true)
            , text(itemText)
            , MenuItemType(EChessMenuItemType::Standard)
        {
        }

        /// Constructor for custom menu items
        FChessMenuItem(TSharedPtr<SWidget> customWidget)
            : IsVisible(true)
            , MenuItemType(EChessMenuItemType::CustomWidget)
            , CustomWidget(customWidget)
        {
        }

        /// Constructor for multi-choice menu items
        FChessMenuItem(FText itemText, TArray<FText> choices, int32 defaultIndex = 0)
            : IsVisible(true)
            , MenuItemType(EChessMenuItemType::MultiChoice)
            , text(itemText)
            , MultiChoice(choices)
            , MinMultiChoiceIndex(-1)
            , SelectedMultiChoiceIndex(defaultIndex)
        {
        }

        const FText& GetText() const { return text; }
        void SetText(const FText& updatedText)
        {
            text = updatedText;
            if (SlateWidget.IsValid())
            {
                SlateWidget->UpdateItemText(text.ToString());
            }
        }

        static TSharedRef<FChessMenuItem> CreateRoot()
        {
            return MakeShareable(new FChessMenuItem());
        }

    private:
        // Private default ctor
        FChessMenuItem()
            : IsVisible(false)
            , MenuItemType(EChessMenuItemType::Root)
        {
        }

    public:
        /// Delegate which is fired by SChessMenuWidget if the user confirms this menu item
        FOnConfirmMenuItem OnConfirmMenuItem;
        /// Delegate which is fired when a multi-choice option is changed. Params are the menu item and the new index
        FOnOptionChanged OnOptionChanged;

        /// The type of this menu item
        EChessMenuItemType::Type MenuItemType;

        /// If this menu item will be created when the menu is opened
        bool IsVisible;

        /// The sub menu for this item (If present)
        TArray<TSharedPtr<FChessMenuItem>> SubMenu;

        /// SharedPtr to the actual Slate widget we are wrapping
        TSharedPtr<SChessMenuItem> SlateWidget;

        /// SharedPtr to the actual slate widget if this menu item is a custom type
        TSharedPtr<SWidget> CustomWidget;

        /// The text for multi-choice menu items
        TArray<FText> MultiChoice;

        /// Set to other then -1 to limit the options range
        int32 MinMultiChoiceIndex;

        /// Set to other value than -1 to limit the options range
        int32 MaxMultiChoiceIndex;

        /// The selected multi-choice index for this menu item
        int32 SelectedMultiChoiceIndex;

    private:
        /// The menu item's text
        FText text;
};
