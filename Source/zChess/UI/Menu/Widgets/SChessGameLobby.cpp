// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "SChessGameLobby.h"
#include "SlateOptMacros.h"
#include "ChessGameInstance.h"
#include "UI/Styles/ChessStyle.h"

#define LOCTEXT_NAMESPACE "ChessGame.HUD.Menu"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChessGameLobby::Construct(const FArguments& InArgs)
{
	playerOwner = InArgs._playerOwner;
    ownerWidget = InArgs._ownerWidget;
    mapFilterName = "Any";
    isSearchingForServers = false;
    isLANMatchSearch = false;
    statusText = FText();
    boxWidth = 125;

    ChildSlot
    .VAlign(VAlign_Fill)
    .HAlign(HAlign_Fill)
    [
        SNew(SVerticalBox)
        + SVerticalBox::Slot()
        .AutoHeight()
        [
            SNew(SBox)
            .WidthOverride(600)
            .HeightOverride(300)
            [
                SAssignNew(onlineGameListWidget, SListView<TSharedPtr<FOnlineGameEntry>>)
                .ItemHeight(20)
                .ListItemsSource(&onlineGameList)
                .SelectionMode(ESelectionMode::Single)
                .OnGenerateRow(this, &SChessGameLobby::MakeListViewWidget)
                .OnSelectionChanged(this, &SChessGameLobby::EntrySelectionChanged)
                .OnMouseButtonDoubleClick(this, &SChessGameLobby::OnListItemDoubleClicked)
                .HeaderRow(
                    SNew(SHeaderRow)
                    + SHeaderRow::Column("GameName").FixedWidth(boxWidth * 2).DefaultLabel(NSLOCTEXT("GameList", "GameNameColumn", "Game Name"))
                    + SHeaderRow::Column("GameType").DefaultLabel(NSLOCTEXT("GameList", "GameTypeColumn", "Game Type"))
                    + SHeaderRow::Column("Players").DefaultLabel(NSLOCTEXT("GameList", "PlayersColumn", "Players"))
                    + SHeaderRow::Column("Ping").DefaultLabel(NSLOCTEXT("GameList", "NetworkPingColumn", "Ping")))
            ]
        ]

        + SVerticalBox::Slot()
        .AutoHeight()
        [
            SNew(SOverlay)
            + SOverlay::Slot()
            .VAlign(VAlign_Center)
            .HAlign(HAlign_Center)
            [
                SNew(STextBlock)
                .Text(this, &SChessGameLobby::GetBottomText)
				//.TextStyle(FChessStyle::Get(), "ChessGame.MenuServerListTextStyle")
            ]
        ]
    ];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SChessGameLobby::OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent)
{
    return FReply::Handled().SetUserFocus(onlineGameListWidget.ToSharedRef(), EFocusCause::SetDirectly).SetUserFocus(SharedThis(this), EFocusCause::SetDirectly, true);
}

void SChessGameLobby::OnFocusLost(const FFocusEvent& InFocusEvent)
{
    if (InFocusEvent.GetCause() != EFocusCause::SetDirectly)
    {
        FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
    }
}

FReply SChessGameLobby::OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent)
{
    if (isSearchingForServers) // Lock input
    {
        return FReply::Handled();
    }

    FReply result = FReply::Unhandled();
    const FKey key = InKeyEvent.GetKey();

    if (key == EKeys::Up)
    {
        MoveSelection(-1);
        result = FReply::Handled();
    }
    else if (key == EKeys::Down)
    {
        MoveSelection(1);
        result = FReply::Handled();
    }
    else if (key == EKeys::Enter)
    {
        ConnectToOnlineGame();
        result = FReply::Handled();
        FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
    }
    else if (key == EKeys::SpaceBar)
    {
        BeginServerSearch(isLANMatchSearch, "Any");
    }

    return result;
}

void SChessGameLobby::OnListItemDoubleClicked(TSharedPtr<FOnlineGameEntry> inItem)
{
    selectedItem = inItem;
    ConnectToOnlineGame();
    FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
}

TSharedRef<ITableRow> SChessGameLobby::MakeListViewWidget(TSharedPtr<FOnlineGameEntry> item, const TSharedRef<STableViewBase>& ownerTable)
{
    class SOnlineGameEntryWidget : public SMultiColumnTableRow<TSharedPtr<FOnlineGameEntry>>
    {
        public:
            SLATE_BEGIN_ARGS(SOnlineGameEntryWidget)
            {
            }
            SLATE_END_ARGS()

            void Construct(const FArguments& InArgs, const TSharedRef<STableViewBase>& InOwnerTable, TSharedPtr<FOnlineGameEntry> InItem)
            {
                item = InItem;
                SMultiColumnTableRow<TSharedPtr<FOnlineGameEntry>>::Construct(FSuperRowType::FArguments(), InOwnerTable);
            }

            TSharedRef<SWidget> GenerateWidgetForColumn(const FName& ColumnName)
            {
                FText itemText;

                if (ColumnName == "GameName")
                {
                    itemText = FText::FromString(item->GameName + "'s Game");
                }
                else if (ColumnName == "GameType")
                {
                    itemText = FText::FromString(item->GameType);
                }
                else if (ColumnName == "Players")
                {
                    const FText playersInGame = FText::Format(FText::FromString("{0}/{1}"), FText::FromString(item->CurrentPlayers), FText::FromString(item->MaxPlayers));
                    itemText = playersInGame;
                }
                else if (ColumnName == "Ping")
                {
                    itemText = FText::FromString(item->Ping);
                }

                return SNew(STextBlock)
                    .Text(itemText);
            }

        public:
            TSharedPtr<FOnlineGameEntry> item;
    };

    return SNew(SOnlineGameEntryWidget, ownerTable, item);
}

void SChessGameLobby::EntrySelectionChanged(TSharedPtr<FOnlineGameEntry> inItem, ESelectInfo::Type selectInfo)
{
    selectedItem = inItem;
}

AChessGameSession* SChessGameLobby::GetGameSession() const
{
    UChessGameInstance* const gameInstance = Cast<UChessGameInstance>(playerOwner->GetGameInstance());
    return gameInstance ? gameInstance->GetGameSession() : nullptr;
}

void SChessGameLobby::UpdateSearchStatus()
{
    check(isSearchingForServers);

    bool finishSearch = true;
    AChessGameSession* chessSession = GetGameSession();

    if (chessSession)
    {
        int32 currentSearchIndex;
        int32 numSearchResults;
        EOnlineAsyncTaskState::Type searchState = chessSession->GetSearchResultStatus(currentSearchIndex, numSearchResults);

        UE_LOG(LogOnlineGame, Log, TEXT("ChessSession->GetSearchResultStatus: %s"), EOnlineAsyncTaskState::ToString(searchState));

        switch (searchState)
        {
            case EOnlineAsyncTaskState::InProgress:
                statusText = LOCTEXT("Searching", "SEARCHING...");
                finishSearch = false;
                break;

            case EOnlineAsyncTaskState::Done:
            {
                onlineGameList.Empty();

                const TArray<FOnlineSessionSearchResult>& searchResults = chessSession->GetSearchResults();
                check(searchResults.Num() == numSearchResults);

                if (numSearchResults == 0)
                {
                    statusText = LOCTEXT("NoOnlineGamesFound", "NO GAMES FOUND, PRESS SPACE TO TRY AGAIN");
                }
                else
                {
                    statusText = LOCTEXT("OnlineGamesRefresh", "PRESS SPACE TO REFRESH GAME LIST");
                }

                for (int32 indexResult = 0; indexResult != numSearchResults; ++indexResult)
                {
                    TSharedPtr<FOnlineGameEntry> newOnlineGameEntry = MakeShareable(new FOnlineGameEntry());
                    const FOnlineSessionSearchResult& result = searchResults[indexResult];

                    newOnlineGameEntry->GameName = result.Session.OwningUserName;
                    newOnlineGameEntry->Ping = FString::FromInt(result.PingInMs);
                    newOnlineGameEntry->CurrentPlayers = FString::FromInt(result.Session.SessionSettings.NumPublicConnections + result.Session.SessionSettings.NumPrivateConnections
                        - result.Session.NumOpenPublicConnections - result.Session.NumOpenPrivateConnections);
                    newOnlineGameEntry->MaxPlayers = FString::FromInt(result.Session.SessionSettings.NumPublicConnections + result.Session.SessionSettings.NumPrivateConnections);
                    newOnlineGameEntry->SearchResultsIndex = indexResult;

                    result.Session.SessionSettings.Get(SETTING_GAMEMODE, newOnlineGameEntry->GameType);
                    result.Session.SessionSettings.Get(SETTING_MAPNAME, newOnlineGameEntry->MapName);

                    onlineGameList.Add(newOnlineGameEntry);
                }
            }
            break;

            case EOnlineAsyncTaskState::Failed:
            case EOnlineAsyncTaskState::NotStarted:
				statusText = FText();
            default:
                break;
        }
    }

    if (finishSearch)
    {
        OnServerSearchFinished();
    }
}

void SChessGameLobby::BeginServerSearch(bool isLANMatch, const FString& inMapFilterName)
{
    isLANMatchSearch = isLANMatch;
    mapFilterName = inMapFilterName;
    isSearchingForServers = true;
    onlineGameList.Empty();

    UChessGameInstance* const gameInstance = Cast<UChessGameInstance>(playerOwner->GetGameInstance());
    if (gameInstance)
    {
        gameInstance->FindSessions(playerOwner.Get(), isLANMatchSearch);
    }
}

void SChessGameLobby::OnServerSearchFinished()
{
    isSearchingForServers = false;
    UpdateOnlineGameList();
}

void SChessGameLobby::UpdateOnlineGameList()
{
    // Filter map if a specific map is specified (Can use this for ranked and non ranked filters)
    if (mapFilterName != "Any")
    {
        for (int32 i = 0; i != onlineGameList.Num(); ++i)
        {
            if (onlineGameList[i]->MapName != mapFilterName)
            {
                onlineGameList.RemoveAt(i);
            }
        }
    }

    int32 selectedItemIndex = onlineGameList.IndexOfByKey(selectedItem);

    onlineGameListWidget->RequestListRefresh();
    if (onlineGameList.Num() > 0)
    {
        onlineGameListWidget->UpdateSelectionSet();
        onlineGameListWidget->SetSelection(onlineGameList[selectedItemIndex > 1 ? selectedItemIndex : 0], ESelectInfo::OnNavigation);
    }
}

void SChessGameLobby::ConnectToOnlineGame()
{
    if (isSearchingForServers)
    {
        return;
    }

    #if WITH_EDITOR
    if (GIsEditor == true)
    {
        return;
    }
    #endif

    if (selectedItem.IsValid())
    {
        int onlineGameToJoin = selectedItem->SearchResultsIndex;

        if (GEngine && GEngine->GameViewport)
        {
            GEngine->GameViewport->RemoveAllViewportWidgets();
        }

        UChessGameInstance* const gameInstance = Cast<UChessGameInstance>(playerOwner->GetGameInstance());
        if (gameInstance)
        {
            gameInstance->JoinSession(playerOwner.Get(), onlineGameToJoin);
        }
    }
}

void SChessGameLobby::MoveSelection(int32 moveBy)
{
    int32 selectedItemIndex = onlineGameList.IndexOfByKey(selectedItem);

    if (selectedItemIndex + moveBy > -1 && selectedItemIndex + moveBy < onlineGameList.Num())
    {
        onlineGameListWidget->SetSelection(onlineGameList[selectedItemIndex + moveBy]);
    }
}

void SChessGameLobby::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
    SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

    if (isSearchingForServers)
    {
        UpdateSearchStatus();
    }
}

FText SChessGameLobby::GetBottomText() const
{
    return statusText;
}

#undef LOCTEXT_NAMESPACE