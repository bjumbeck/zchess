// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "SChessMenuItem.h"
#include "SlateOptMacros.h"
#include "UI/Styles/ChessStyle.h"
#include "UI/Styles/ChessMenuItemWidgetStyle.h"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChessMenuItem::Construct(const FArguments& InArgs)
{
    menuItemStyle = &FChessStyle::Get().GetWidgetStyle<FChessMenuItemStyle>("DefaultChessMenuItemStyle");

    // Setup our member variables
    playerOwner = InArgs._playerOwner;

    LeftArrowVisible = EVisibility::Collapsed;
    RightArrowVisible = EVisibility::Collapsed;

    text = InArgs._text;
    optionText = InArgs._optionText;
    isMultiChoiceMenuItem = InArgs._isMultiChoiceMenuItem;
    isActiveMenuItem = false;
    inactiveTextAlpha = InArgs._inactiveTextAlpha.Get(1.0f);
    
    onClicked = InArgs._onClicked;
    onArrowPressed = InArgs._onArrowPressed;

    const float arrowMargin = 3.0f;
    itemMargin = 10.0f;
    textColor = FLinearColor(FColor(155, 164, 182));

    // Construct the menu item UI
    ChildSlot
    .VAlign(VAlign_Fill)
    .HAlign(HAlign_Fill)
    [
        SNew(SOverlay)
        + SOverlay::Slot()
        .HAlign(HAlign_Fill)
        .VAlign(VAlign_Fill)
        [
            // Menu Item Background
            SNew(SBox)
            .WidthOverride(374.0f)
            .HeightOverride(23.0f)
            [
                SNew(SImage)
                .ColorAndOpacity(this, &SChessMenuItem::GetButtonBackgroundColor)
                .Image(&menuItemStyle->BackgroundBrush)
            ]
        ]
        + SOverlay::Slot()
        .HAlign(isMultiChoiceMenuItem ? HAlign_Left : HAlign_Center)
        .VAlign(VAlign_Center)
        .Padding(FMargin(itemMargin, 0, 0, 0))
        [
            // Menu Item Main Text
            SAssignNew(textWidget, STextBlock)
            .TextStyle(&menuItemStyle->MainTextStyle)
            .ColorAndOpacity(this, &SChessMenuItem::GetButtonTextColor)
            .ShadowColorAndOpacity(this, &SChessMenuItem::GetButtonTextShadowColor)
            .Text(text)
        ]
        + SOverlay::Slot()
        .HAlign(HAlign_Right)
        .VAlign(VAlign_Center)
        [
            SNew(SHorizontalBox)
            + SHorizontalBox::Slot()
            .AutoWidth()
            [
                // Left Arrow
                SNew(SBorder)
                .BorderImage(FCoreStyle::Get().GetBrush("NoBorder"))
                .Padding(FMargin(0, 0, arrowMargin, 0))
                .Visibility(this, &SChessMenuItem::GetLeftArrowVisibility)
                .OnMouseButtonDown(this, &SChessMenuItem::OnLeftArrowDown)
                [
                    SNew(SOverlay)
                    + SOverlay::Slot()
                    .HAlign(HAlign_Center)
                    .VAlign(VAlign_Center)
                    [
                        SNew(SImage)
                        .Image(&menuItemStyle->LeftArrowImage)
                    ]
                ]
            ]
            + SHorizontalBox::Slot()
            .AutoWidth()
            .Padding(TAttribute<FMargin>(this, &SChessMenuItem::GetOptionPadding))
            [
                // Option 
                SNew(STextBlock)
                .TextStyle(&menuItemStyle->OptionTextStyle)
                .Visibility(isMultiChoiceMenuItem ? EVisibility::Visible : EVisibility::Collapsed)
                .ColorAndOpacity(this, &SChessMenuItem::GetButtonTextColor)
                .ShadowColorAndOpacity(this, &SChessMenuItem::GetButtonTextShadowColor)
                .Text(optionText)
            ]
            + SHorizontalBox::Slot()
            .AutoWidth()
            [
                // Right Arrow
                SNew(SBorder)
                .BorderImage(FCoreStyle::Get().GetBrush("NoBorder"))
                .Padding(FMargin(arrowMargin, 0, itemMargin, 0))
                .Visibility(this, &SChessMenuItem::GetRightArrowVisibility)
                .OnMouseButtonDown(this, &SChessMenuItem::OnRightArrowDown)
                [
                    SNew(SOverlay)
                    + SOverlay::Slot()
                    .HAlign(HAlign_Center)
                    .VAlign(VAlign_Center)
                    [
                        SNew(SImage)
                        .Image(&menuItemStyle->RightArrowImage)
                    ]
                ]
            ]
        ]
    ];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

FReply SChessMenuItem::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
    // Execute our OnClicked delegate (If we have one)
    if (onClicked.IsBound())
    {
        return onClicked.Execute();
    }

    return FReply::Handled();
}

FReply SChessMenuItem::OnMouseButtonUp(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
    return FReply::Handled();
}

void SChessMenuItem::SetMenuItemActive(bool isMenuItemActive)
{
    isActiveMenuItem = isMenuItemActive;
}

void SChessMenuItem::UpdateItemText(const FString& updatedText)
{
    text = updatedText;
    if (textWidget.IsValid())
    {
        textWidget->SetText(text);
    }
}

FSlateColor SChessMenuItem::GetButtonBackgroundColor() const
{
    const float minAlpha = 0.1f;
    const float maxAlpha = 1.0f;
    const float animSpeedMod = 1.5f;

    float animPercent = 0.0f;
    ULocalPlayer* const player = playerOwner.Get();
    if (player)
    {
        // Hack: Do we really need a world object to get delta time?
        UWorld* const world = player->GetWorld();
        if (world)
        {
            const float gameTime = world->GetRealTimeSeconds();
            animPercent = FMath::Abs(FMath::Sin(gameTime * animSpeedMod));
        }
    }

    const float backgroundAlpha = isActiveMenuItem ? FMath::Lerp(minAlpha, maxAlpha, animPercent) : 0.0f;
    return FLinearColor(1.0f, 1.0f, 1.0f, backgroundAlpha);
}

FSlateColor SChessMenuItem::GetButtonTextColor() const
{
    FLinearColor result;

    if (isActiveMenuItem)
    {
        result = textColor;
    }
    else
    {
        result = FLinearColor(textColor.R, textColor.G, textColor.B, inactiveTextAlpha);
    }

    return result;
}

FLinearColor SChessMenuItem::GetButtonTextShadowColor() const
{
    FLinearColor result;

    if (isActiveMenuItem)
    {
        result = FLinearColor(0, 0, 0, 1);
    }
    else
    {
        result = FLinearColor(0, 0, 0, inactiveTextAlpha);
    }

    return result;
}

EVisibility SChessMenuItem::GetRightArrowVisibility() const
{
    return RightArrowVisible;
}

EVisibility SChessMenuItem::GetLeftArrowVisibility() const
{
    return LeftArrowVisible;
}

FMargin SChessMenuItem::GetOptionPadding() const
{
    return RightArrowVisible == EVisibility::Visible ? FMargin(0) : FMargin(0, 0, itemMargin, 0);
}

FReply SChessMenuItem::OnRightArrowDown(const FGeometry& myGeometry, const FPointerEvent& mouseEvent)
{
    FReply result = FReply::Unhandled();
    const int32 moveRight = 1;

    if (onArrowPressed.IsBound() && isActiveMenuItem)
    {
        onArrowPressed.Execute(moveRight);
        result = FReply::Handled();
    }

    return result;
}

FReply SChessMenuItem::OnLeftArrowDown(const FGeometry& myGeometry, const FPointerEvent& mouseEvent)
{
    FReply result = FReply::Unhandled();
    const int32 moveLeft = -1;

    if (onArrowPressed.IsBound() && isActiveMenuItem)
    {
        onArrowPressed.Execute(moveLeft);
        result = FReply::Handled();
    }

    return result;
}