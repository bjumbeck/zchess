// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Widgets/SCompoundWidget.h"
#include "ChessMenuItem.h"


class ZCHESS_API SChessMenuWidget : public SCompoundWidget
{
    public:
	    SLATE_BEGIN_ARGS(SChessMenuWidget)
            : _playerOwner()
            , _isInGameMenu(false)
	    {}

        SLATE_ARGUMENT(TWeakObjectPtr<ULocalPlayer>, playerOwner)
        SLATE_ARGUMENT(bool, isInGameMenu)

	    SLATE_END_ARGS()

        // Delegates
        DECLARE_DELEGATE(FOnMenuHidden);
        DECLARE_DELEGATE_OneParam(FOnMenuGoBack, MenuPtrType);

	    /** Constructs this widget with InArgs */
	    void Construct(const FArguments& InArgs);

        /// Update function, this is needed to allow us to only start fading in once we are done loading
        virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

        /// To have the mouse cursor show up at all times, we need the widget to handle all mouse events
        virtual FReply OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent) override;

        /// Key down handler
        virtual FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) override;

        /// Support keyboard focus
        virtual bool SupportsKeyboardFocus() const override { return true; }

        /// The menu sets up the appropriate mouse settings upon focus
        virtual FReply OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent) override;

        /// Setup animation lengths, start points and launch initial animation
        void SetupAnimations();

        /// Build left menu panel
        void BuildLeftPanel(bool goingBack);

        /// Build inactive next menu panel (Current selection's sub-menu preview)
        void BuildRightPanel();

        /// Start the animations to enter the sub-menu, it will then become active menu
        void EnterSubMenu();

        /// Starts reverse animations to go one level up in the menu hierarchy
        void MenuGoBack(bool silent = false);

        /// Confirms the current menu item and performs an action
        void ConfirmMenuItem();

        /// Call to rebuild the menu and start animating it
        void BuildAndShowMenu();

        /// Call to hide the menu
        void HideMenu();

        /// Update arrow's visibility for multi-choice items
        void UpdateArrows(TSharedPtr<FChessMenuItem> menuItem);

        /// Changes option in multi-choice items
        void ChangeOption(int32 moveBy);

        /// Get the next valid index (Ignores invisible items)
        int32 GetNextValidIndex(int32 moveBy);

        /// Disable and enable moving around the menu
        void LockControls(bool enable);

        /// Cache the UserIndex from the owning PlayerController
        int32 GetOwnerUserIndex();

        /// Returns the number of sub levels on the menu stack
        int32 GetMenuLevel();

    private:
        /// This function starts the entire fade in process
        void FadeIn();

        /// Set hit test invisibility when console is up
        EVisibility GetSlateVisibility() const;

        /// Getters used for animating the menu
        FVector2D GetBottomScale() const;
        FLinearColor GetBottomColor() const;
        FLinearColor GetTopColor() const;
        FMargin GetMenuOffset() const;
        FMargin GetLeftMenuOffset() const;
        FMargin GetSubMenuOffset() const;

        /// Gets the header image color
        FSlateColor GetHeaderColor() const;

        /// Callback for when one of the N buttons is clicked
        FReply ButtonClicked(int32 buttonIndex);

        /// Get the currently selected multi-choice option
        FString GetOptionText(TSharedPtr<FChessMenuItem> menuItem) const;

        /// Gets the current menu title string
        FText GetMenuTitle() const;

    public:
        /// The menu implementation for this instance of the widget
        MenuPtrType MainMenu;

        /// The currently active menu
        MenuPtrType CurrentMenu;

        /// The next menu (For transition and displaying as the right menu)
        MenuPtrType NextMenu;

        /// A stack of previous menus
        TArray<FChessMenuInfo> MenuHistory;

        /// The delegate which is fired when the menu is finished hiding
        FOnMenuHidden OnMenuHidden;

        /// The delegate which is fired when the user wishes to return the the previous menu
        FOnMenuGoBack OnMenuGoBack;

        // Current Menu Title (If present)
        FText CurrentMenuTitle;

        /// Is the in game console visible
        bool isConsoleVisible;

    private:
        /// Weak pointer to the parent PC
        TWeakObjectPtr<class ULocalPlayer> playerOwner;

        /// The style for the menu widget
        const struct FChessMenuStyle* menuStyle;

        /// Flag to determine if this is a in game menu
        bool isInGameMenu;

        /// Menu that will override the current one after transition animation
        MenuPtrType pendingLeftMenu;

        /// Left(Current) menu layout box
        TSharedPtr<SVerticalBox> leftBox;

        /// Right(Sub) menu layout box
        TSharedPtr<SVerticalBox> rightBox;

        /// If moving around the menu is currently locked
        bool isControlsLocked;

        /// The screen resolution
        FIntPoint screenRes;

        // Spacing and menu dimensions
        float outlineWidth;
        float menuHeaderHeight;
        float menuHeaderWidth;

        /// Animation type index
        int32 animationNumber;

        /// Selected index of the current menu
        int32 selectedIndex;

        // Animation Stuff
        
        /// Sub menu animating flag
        bool isSubmenuChanging;

        /// Left menu animating flag
        bool isLeftMenuChanging;

        /// Going back to the previous menu animation flag
        bool isGoingBack;

        /// Menu hiding animation flag
        bool isMenuHiding;

        /// The curve sequence for the menu animation and the related handles
        FCurveSequence menuWidgetAnimation;
        FCurveHandle bottomScaleYCurve; // Used for menu background scaling animation at the beginning
        FCurveHandle topColorCurve; // Used for the main menu logo fade in animation at the beginning
        FCurveHandle bottomColorCurve; // Used for the menu background fade in animation at the beginning
        FCurveHandle buttonsPosXCurve; // Used for the menu buttons slide in animation at the beginning

        /// The curve sequence for the sub menu transition animation and the related handles
        FCurveSequence subMenuWidgetAnimation;
        FCurveHandle subMenuScrollOutCurve;

        /// The curve sequence for the current menu transition animation
        FCurveSequence leftMenuWidgetAnimation;
        FCurveHandle leftMenuScrollOutCurve;
};

// Below is helper functions for the menus.
// TODO: Maybe find a better place to put this?

namespace MenuHelper
{
    FORCEINLINE void EnsureValid(TSharedPtr<FChessMenuItem>& menuItem)
    {
        if (!menuItem.IsValid())
        {
            menuItem = FChessMenuItem::CreateRoot();
        }
    }

    // Helper functions for creating menu items
    //////////////////////////////////////////////////////////////////////////

    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuItem(TSharedPtr<FChessMenuItem>& menuItem, const FText& text)
    {
        EnsureValid(menuItem);
        TSharedPtr<FChessMenuItem> item = MakeShareable(new FChessMenuItem(text));
        menuItem->SubMenu.Add(item);
        
        return item.ToSharedRef();
    }

    /// Add standard item to the menu with UObject delegate
    template <class UserClass>
    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuItem(TSharedPtr<FChessMenuItem>& menuItem, const FText& text, UserClass* inObj, typename FChessMenuItem::FOnConfirmMenuItem::TUObjectMethodDelegate<UserClass>::FMethodPtr inMethod)
    {
        EnsureValid(menuItem);
        TSharedPtr<FChessMenuItem> item = MakeShareable(new FChessMenuItem(text));
        item->OnConfirmMenuItem.BindUObject(inObj, inMethod);
        menuItem->SubMenu.Add(item);

        return item.ToSharedRef();
    }

    /// Add standard item to menu with TSharedPtr delegate
    template<class UserClass>
    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuItemSP(TSharedPtr<FChessMenuItem>& MenuItem, const FText& Text, UserClass* inObj, typename FChessMenuItem::FOnConfirmMenuItem::TSPMethodDelegate< UserClass >::FMethodPtr inMethod)
    {
        EnsureValid(MenuItem);
        TSharedPtr<FChessMenuItem> Item = MakeShareable(new FChessMenuItem(Text));
        Item->OnConfirmMenuItem.BindSP(inObj, inMethod);
        MenuItem->SubMenu.Add(Item);
        return Item.ToSharedRef();
    }

    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuOption(TSharedPtr<FChessMenuItem>& MenuItem, const FText& Text, const TArray<FText>& OptionsList)
    {
        EnsureValid(MenuItem);
        TSharedPtr<FChessMenuItem> Item = MakeShareable(new FChessMenuItem(Text, OptionsList));
        MenuItem->SubMenu.Add(Item);
        return MenuItem->SubMenu.Last().ToSharedRef();
    }

    /** add multi-choice item to menu with UObject delegate */
    template< class UserClass >
    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuOption(TSharedPtr<FChessMenuItem>& MenuItem, const FText& Text, const TArray<FText>& OptionsList, UserClass* inObj, typename FChessMenuItem::FOnOptionChanged::TUObjectMethodDelegate< UserClass >::FMethodPtr inMethod)
    {
        EnsureValid(MenuItem);
        TSharedPtr<FChessMenuItem> Item = MakeShareable(new FChessMenuItem(Text, OptionsList));
        Item->OnOptionChanged.BindUObject(inObj, inMethod);
        MenuItem->SubMenu.Add(Item);
        return MenuItem->SubMenu.Last().ToSharedRef();
    }

    /** add multi-choice item to menu with TSharedPtr delegate */
    template< class UserClass >
    FORCEINLINE TSharedRef<FChessMenuItem> AddMenuOptionSP(TSharedPtr<FChessMenuItem>& MenuItem, const FText& Text, const TArray<FText>& OptionsList, UserClass* inObj, typename FChessMenuItem::FOnOptionChanged::TSPMethodDelegate< UserClass >::FMethodPtr inMethod)
    {
        EnsureValid(MenuItem);
        TSharedPtr<FChessMenuItem> Item = MakeShareable(new FChessMenuItem(Text, OptionsList));
        Item->OnOptionChanged.BindSP(inObj, inMethod);
        MenuItem->SubMenu.Add(Item);
        return MenuItem->SubMenu.Last().ToSharedRef();
    }


    FORCEINLINE TSharedRef<FChessMenuItem> AddExistingMenuItem(TSharedPtr<FChessMenuItem>& MenuItem, TSharedRef<FChessMenuItem> SubMenuItem)
    {
        EnsureValid(MenuItem);
        MenuItem->SubMenu.Add(SubMenuItem);
        return MenuItem->SubMenu.Last().ToSharedRef();
    }


    FORCEINLINE TSharedRef<FChessMenuItem> AddCustomMenuItem(TSharedPtr<FChessMenuItem>& MenuItem, TSharedPtr<SWidget> CustomWidget)
    {
        EnsureValid(MenuItem);
        MenuItem->SubMenu.Add(MakeShareable(new FChessMenuItem(CustomWidget)));
        return MenuItem->SubMenu.Last().ToSharedRef();
    }

    FORCEINLINE void ClearSubMenu(TSharedPtr<FChessMenuItem>& MenuItem)
    {
        EnsureValid(MenuItem);
        MenuItem->SubMenu.Empty();
    }

    template< class UserClass >
    FORCEINLINE void PlaySoundAndCall(UWorld* World, const FSlateSound& Sound, int32 UserIndex, UserClass* inObj, typename FChessMenuItem::FOnConfirmMenuItem::TSPMethodDelegate< UserClass >::FMethodPtr inMethod)
    {
        FSlateApplication::Get().PlaySound(Sound, UserIndex);
        if (World)
        {
            const float SoundDuration = FMath::Max(FSlateApplication::Get().GetSoundDuration(Sound), 0.1f);
            FTimerHandle DummyHandle;
            World->GetTimerManager().SetTimer(DummyHandle, FTimerDelegate::CreateSP(inObj, inMethod), SoundDuration, false);
        }
        else
        {
            FTimerDelegate D = FTimerDelegate::CreateSP(inObj, inMethod);
            D.ExecuteIfBound();
        }
    }
};