// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"
#include "SChessMenuWidget.h"
#include "zChess.h"
#include "Online/ChessGameSession.h"

struct FOnlineGameEntry
{
    FString GameName;
    FString CurrentPlayers;
    FString MaxPlayers;
    FString GameType;
    FString MapName; // Used for Ranked/Unranked/Etc.
    FString Ping;
    int32 SearchResultsIndex;
};

class ZCHESS_API SChessGameLobby : public SChessMenuWidget
{
    public:
	    SLATE_BEGIN_ARGS(SChessGameLobby)
	    {}

        SLATE_ARGUMENT(TWeakObjectPtr<ULocalPlayer>, playerOwner)
        SLATE_ARGUMENT(TSharedPtr<SWidget>, ownerWidget)

	    SLATE_END_ARGS()

	    /** Constructs this widget with InArgs */
	    void Construct(const FArguments& InArgs);

        virtual bool SupportsKeyboardFocus() const override { return true; }

        // Used to keep action bindings list focused
        virtual FReply OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent) override;

        // Used to keep action bindings list focused
        virtual void OnFocusLost(const FFocusEvent& InFocusEvent) override;

        // Key down handler
        virtual FReply OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent) override;

        // SListView item double clicked
        void OnListItemDoubleClicked(TSharedPtr<FOnlineGameEntry> inItem);

        // Creates single item widget, called for every list item
        TSharedRef<ITableRow> MakeListViewWidget(TSharedPtr<FOnlineGameEntry> item, const TSharedRef<STableViewBase>& ownerTable);

        // Selection changed handler
        void EntrySelectionChanged(TSharedPtr<FOnlineGameEntry> inItem, ESelectInfo::Type selectInfo);

        // Get the current game session
        AChessGameSession* GetGameSession() const;

        // Updates the current search status
        void UpdateSearchStatus();

        // Starts searching for servers
        void BeginServerSearch(bool isLANMatch, const FString& inMapFilterName);

        // Called when Online Game search is finished
        void OnServerSearchFinished();

        // Fill/Update Online Game list
        void UpdateOnlineGameList();

        // Connect to the chosen game
        void ConnectToOnlineGame();

        // Selects item at current + moveby index
        void MoveSelection(int32 moveBy);

		virtual void Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime) override;

    protected:
        FText GetBottomText() const;

    protected:
        /// Whether last searched for LAN
        bool isLANMatchSearch;

        /// Whether searching for online servers
        bool isSearchingForServers;

        /// Action bindings array
        TArray<TSharedPtr<FOnlineGameEntry>> onlineGameList;

        /// Action bindings list slate widget
        TSharedPtr<SListView<TSharedPtr<FOnlineGameEntry>>> onlineGameListWidget;

        /// Currently selected list item
        TSharedPtr<FOnlineGameEntry> selectedItem;

        /// Current status text
        FText statusText;

        /// Map filter name to use during server searches
        FString mapFilterName;

        /// Size of the standard column in pixels
        int32 boxWidth;

        /// Pointer to our owner PC
        TWeakObjectPtr<class ULocalPlayer> playerOwner;

        /// Pointer to our parent widget
        TSharedPtr<class SWidget> ownerWidget;
};
