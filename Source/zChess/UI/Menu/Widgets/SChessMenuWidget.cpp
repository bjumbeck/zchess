// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "SChessMenuWidget.h"
#include "SlateOptMacros.h"
#include "ChessMenuItem.h"
#include "UI/Styles/ChessStyle.h"
#include "UI/Styles/ChessMenuWidgetStyle.h"
#include "Engine/Console.h"

#define LOCTEXT_NAMESPACE "SChessMenuWidget"

BEGIN_SLATE_FUNCTION_BUILD_OPTIMIZATION
void SChessMenuWidget::Construct(const FArguments& InArgs)
{
    menuStyle = &FChessStyle::Get().GetWidgetStyle<FChessMenuStyle>("DefaultChessMenuStyle");

    isControlsLocked = false;
    isConsoleVisible = false;
    outlineWidth = 20.0f;
    selectedIndex = 0;
    playerOwner = InArgs._playerOwner;
    isInGameMenu = InArgs._isInGameMenu;
    Visibility.Bind(this, &SChessMenuWidget::GetSlateVisibility);
    FLinearColor menuTitleTextColor = FLinearColor(FColor(155, 164, 182));
    menuHeaderHeight = 62.0f;
    menuHeaderWidth = 287.0f;

    ChildSlot
    [
		SNew(SOverlay)
        + SOverlay::Slot()
        .VAlign(VAlign_Fill)
        .HAlign(HAlign_Fill)
        [
            SNew(SVerticalBox)
            + SVerticalBox::Slot()
            .HAlign(HAlign_Left)
            .VAlign(VAlign_Top)
            .Padding(TAttribute<FMargin>(this, &SChessMenuWidget::GetMenuOffset))
            [
                // Menu Title
                SNew(SVerticalBox)
                + SVerticalBox::Slot()
                .AutoHeight()
                [
                    SNew(SOverlay)
                    + SOverlay::Slot()
                    .HAlign(HAlign_Left)
                    .VAlign(VAlign_Fill)
                    [
                        SNew(SBox)
                        .WidthOverride(menuHeaderWidth)
                        .HeightOverride(menuHeaderHeight)
                        [
                            SNew(SImage)
                            .ColorAndOpacity(this, &SChessMenuWidget::GetHeaderColor)
                            .Image(&menuStyle->HeaderBackgroundBrush)
                        ]
                    ]
                    + SOverlay::Slot()
                    .HAlign(HAlign_Left)
                    .VAlign(VAlign_Fill)
                    [
                        SNew(SBox)
                        .WidthOverride(menuHeaderWidth)
                        .HeightOverride(menuHeaderHeight)
                        .VAlign(VAlign_Center)
                        .HAlign(HAlign_Center)
                        [
							SNew(STextBlock)
                            .TextStyle(&menuStyle->TitleTextStyle)
                            .ColorAndOpacity(menuTitleTextColor)
                            .Text(this, &SChessMenuWidget::GetMenuTitle)
                        ]
                    ]
                ]

                + SVerticalBox::Slot()
                .AutoHeight()
                [
                    // Menu Portion (Both Main and Sub)
                    SNew(SBorder)
                    .BorderImage(FCoreStyle::Get().GetBrush("NoBorder"))
                    .ColorAndOpacity(this, &SChessMenuWidget::GetBottomColor)
                    .VAlign(VAlign_Top)
                    .HAlign(HAlign_Left)
                    [
                        SNew(SHorizontalBox)
                        + SHorizontalBox::Slot()
                        .AutoWidth()
                        [
                            // Left Menu (Main)
                            SNew(SVerticalBox)
                            + SVerticalBox::Slot()
                            .AutoHeight()
                            .Padding(TAttribute<FMargin>(this, &SChessMenuWidget::GetLeftMenuOffset))
                            [
                                SNew(SBorder)
                                .BorderImage(&menuStyle->LeftBackgroundBrush)
                                .BorderBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f))
                                .Padding(FMargin(outlineWidth))
                                .DesiredSizeScale(this, &SChessMenuWidget::GetBottomScale)
                                .VAlign(VAlign_Top)
                                .HAlign(HAlign_Left)
                                [
                                    SAssignNew(leftBox, SVerticalBox)
                                ]
                            ]
                        ]

                        + SHorizontalBox::Slot()
                        .AutoWidth()
                        [
                            // Right Menu (Sub)
                            SNew(SVerticalBox)
                            + SVerticalBox::Slot()
                            .Padding(TAttribute<FMargin>(this, &SChessMenuWidget::GetSubMenuOffset))
                            .AutoHeight()
                            [
                                SNew(SBorder)
                                .BorderImage(&menuStyle->RightBackgroundBrush)
                                .BorderBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f))
                                .Padding(FMargin(outlineWidth))
                                .DesiredSizeScale(this, &SChessMenuWidget::GetBottomScale)
                                .VAlign(VAlign_Top)
                                .HAlign(HAlign_Left)
                                [
                                    SAssignNew(rightBox, SVerticalBox)
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];
}
END_SLATE_FUNCTION_BUILD_OPTIMIZATION

void SChessMenuWidget::Tick(const FGeometry& AllottedGeometry, const double InCurrentTime, const float InDeltaTime)
{
    SCompoundWidget::Tick(AllottedGeometry, InCurrentTime, InDeltaTime);

    // See if console is open
    UConsole* viewportConsole = (GEngine != NULL && GEngine->GameViewport != NULL) ? GEngine->GameViewport->ViewportConsole : NULL;
    if (viewportConsole != NULL && (viewportConsole->ConsoleState == "Typing" || viewportConsole->ConsoleState == "Open"))
    {
        if (!isConsoleVisible)
        {
            isConsoleVisible = true;
            FSlateApplication::Get().SetAllUserFocusToGameViewport();
        }
    }
    else
    {
        if (isConsoleVisible)
        {
            isConsoleVisible = false;
            FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
        }
    }

    // Get the resolution size
    if (GEngine && GEngine->GameViewport && GEngine->GameViewport->ViewportFrame)
    {
        FViewport* viewport = GEngine->GameViewport->ViewportFrame->GetViewport();
        if (viewport)
        {
            screenRes = viewport->GetSizeXY();
        }
    }

    if (menuWidgetAnimation.IsAtStart() && !isMenuHiding)
    {
        // Start the menu widget animation, set keyboard focus
        FadeIn();
    }
    else if (menuWidgetAnimation.IsAtStart() && isMenuHiding)
    {
        isMenuHiding = false;
        OnMenuHidden.ExecuteIfBound();
    }

    if (menuWidgetAnimation.IsAtEnd())
    {
        if (isLeftMenuChanging)
        {   
            if (leftMenuWidgetAnimation.IsAtEnd())
            {
                pendingLeftMenu = NextMenu;
                if (NextMenu.Num() > 0 && NextMenu.Top()->SubMenu.Num() > 0)
                {
                    NextMenu = NextMenu.Top()->SubMenu;
                }
                else
                {
                    NextMenu.Reset();
                }

                isSubmenuChanging = true;
                leftMenuWidgetAnimation.PlayReverse(SharedThis(this), false); // TODO: Delete Arguments if it fails
            }

            if (!leftMenuWidgetAnimation.IsPlaying())
            {
                if (CurrentMenu.Num() > 0)
                {
                    BuildLeftPanel(isGoingBack);
                    leftMenuWidgetAnimation.Play(SharedThis(this), false); // TODO: Delete Arguments if it fails
                }

                // Focus custom widget
                if (CurrentMenu.Num() == 1 && CurrentMenu.Top()->MenuItemType == EChessMenuItemType::CustomWidget)
                {
                     FSlateApplication::Get().SetKeyboardFocus(CurrentMenu.Top()->CustomWidget);  
                }

                isLeftMenuChanging = false;
                rightBox->ClearChildren();
            }
        }

        if (isSubmenuChanging)
        {
            if (subMenuWidgetAnimation.IsAtEnd())
            {
                subMenuWidgetAnimation.PlayReverse(SharedThis(this), false); // TODO: Delete Arguments if it fails
            }

            if (!subMenuWidgetAnimation.IsPlaying())
            {
                if (NextMenu.Num() > 0)
                {
                    BuildRightPanel();
                    subMenuWidgetAnimation.Play(SharedThis(this), false); // TODO: Delete Arguments if it fails
                }

                isSubmenuChanging = false;
            }
        }
    }
}

FReply SChessMenuWidget::OnMouseButtonDown(const FGeometry& MyGeometry, const FPointerEvent& MouseEvent)
{
    // If we clicked anywhere, to the end of the animation
    if (menuWidgetAnimation.IsPlaying())
    {
        menuWidgetAnimation.JumpToEnd();
    }

    // Set the keyboard focus
    return FReply::Handled().SetUserFocus(SharedThis(this), EFocusCause::SetDirectly);
}

FReply SChessMenuWidget::OnKeyDown(const FGeometry& MyGeometry, const FKeyEvent& InKeyEvent)
{
    FReply result = FReply::Unhandled();
    const int32 userIndex = InKeyEvent.GetUserIndex();
    bool canEventUserInteract = GetOwnerUserIndex() == -1 || userIndex == GetOwnerUserIndex();

    if (!isControlsLocked && canEventUserInteract)
    {   
        const FKey key = InKeyEvent.GetKey();

        // Up Key
        if (key == EKeys::Up)
        {
            int32 nextValidIndex = GetNextValidIndex(-1);
            if (nextValidIndex != selectedIndex)
            {
                ButtonClicked(nextValidIndex);
            }

            result = FReply::Handled();
        }
        // Down Key
        else if (key == EKeys::Down)
        {
            int32 nextValidIndex = GetNextValidIndex(1);
            if (nextValidIndex != selectedIndex)
            {
                ButtonClicked(nextValidIndex);
            }

            result = FReply::Handled();
        }
        // Left Key
        else if (key == EKeys::Left)
        {
            ChangeOption(-1);
            result = FReply::Handled();
        }
        // Right Key
        else if (key == EKeys::Right)
        {
            ChangeOption(1);
            result = FReply::Handled();
        }
        // Enter Key
        else if (key == EKeys::Enter)
        {
            ConfirmMenuItem();
            result = FReply::Handled();
        }
        // Escape Key
        else if (key == EKeys::Escape && !InKeyEvent.IsRepeat())
        {
            MenuGoBack();
            result = FReply::Handled();
        }
    }

    return result;
}

FReply SChessMenuWidget::OnFocusReceived(const FGeometry& MyGeometry, const FFocusEvent& InFocusEvent)
{
    // Focus the custom widget
    if (CurrentMenu.Num() == 1 && CurrentMenu.Top()->MenuItemType == EChessMenuItemType::CustomWidget)
    {
        return FReply::Handled().SetUserFocus(CurrentMenu.Top()->CustomWidget.ToSharedRef(), EFocusCause::SetDirectly);
    }

    return FReply::Handled().ReleaseMouseCapture().SetUserFocus(SharedThis(this), EFocusCause::SetDirectly, true);
}

void SChessMenuWidget::SetupAnimations()
{
    // Setup a curve
    const float startDelay = 0.0f;
    const float secondDelay = isInGameMenu ? 0.0f : 0.3f;
    const float animationDuration = 0.5f;
    const float menuChangeDuration = 0.2f;

    // Always animate the menu from the same side of the screen
    animationNumber = 1;

    menuWidgetAnimation = FCurveSequence();
    subMenuWidgetAnimation = FCurveSequence();
    subMenuScrollOutCurve = subMenuWidgetAnimation.AddCurve(0, menuChangeDuration, ECurveEaseFunction::QuadInOut);

    menuWidgetAnimation = FCurveSequence();
    leftMenuWidgetAnimation = FCurveSequence();
    leftMenuScrollOutCurve = leftMenuWidgetAnimation.AddCurve(0, menuChangeDuration, ECurveEaseFunction::QuadInOut);
    leftMenuWidgetAnimation.Play(SharedThis(this), false); // TODO: Delete Arguments if it fails

    // Define fade in animation
    topColorCurve = menuWidgetAnimation.AddCurve(startDelay, animationDuration, ECurveEaseFunction::QuadInOut);

    // These will animation later

    // rolling out
    bottomScaleYCurve = menuWidgetAnimation.AddCurve(startDelay + secondDelay, animationDuration, ECurveEaseFunction::QuadInOut);
    // fading in
    bottomColorCurve = menuWidgetAnimation.AddCurve(startDelay + secondDelay, animationDuration, ECurveEaseFunction::QuadInOut);
    // Moving from left side off screen
    buttonsPosXCurve = menuWidgetAnimation.AddCurve(startDelay + secondDelay, animationDuration, ECurveEaseFunction::QuadInOut);
}

void SChessMenuWidget::BuildLeftPanel(bool goingBack)
{
    if (CurrentMenu.Num() == 0)
    {
        // Don't build anything if we don't have any active menu
        return;
    }

    leftBox->ClearChildren();
    int32 previousIndex = -1;
    if (isLeftMenuChanging)
    {
        // If we are going back to a previous menu
        if (isGoingBack && MenuHistory.Num() > 0)
        {
            FChessMenuInfo menuInfo = MenuHistory.Pop();
            CurrentMenu = menuInfo.Menu;
            CurrentMenuTitle = menuInfo.MenuTitle;
            previousIndex = menuInfo.SelectedIndex;

            if (CurrentMenu.Num() > 0 && CurrentMenu[previousIndex]->SubMenu.Num() > 0)
            {
                NextMenu = CurrentMenu[previousIndex]->SubMenu;
                isSubmenuChanging = true;
            }
        }
        else if (pendingLeftMenu.Num() > 0)
        {
            MenuHistory.Push(FChessMenuInfo(CurrentMenu, selectedIndex, CurrentMenuTitle));
            CurrentMenuTitle = CurrentMenu[selectedIndex]->GetText();
            CurrentMenu = pendingLeftMenu;
        }
    }

    selectedIndex = previousIndex;

    // Setup the menu buttons
    for (int32 i = 0; i != CurrentMenu.Num(); ++i)
    {
        if (CurrentMenu[i]->IsVisible)
        {
            TSharedPtr<SWidget> tempWidget;
            if (CurrentMenu[i]->MenuItemType == EChessMenuItemType::Standard)
            {
                tempWidget = SAssignNew(CurrentMenu[i]->SlateWidget, SChessMenuItem)
                    .playerOwner(this->playerOwner)
                    .onClicked(this, &SChessMenuWidget::ButtonClicked, i)
                    .text(CurrentMenu[i]->GetText().ToString())
                    .isMultiChoiceMenuItem(false);
            }
            else if (CurrentMenu[i]->MenuItemType == EChessMenuItemType::MultiChoice)
            {
                tempWidget = SAssignNew(CurrentMenu[i]->SlateWidget, SChessMenuItem)
                    .playerOwner(this->playerOwner)
                    .onClicked(this, &SChessMenuWidget::ButtonClicked, i)
                    .text(CurrentMenu[i]->GetText().ToString())
                    .isMultiChoiceMenuItem(true)
                    .onArrowPressed(this, &SChessMenuWidget::ChangeOption)
                    .optionText(this, &SChessMenuWidget::GetOptionText, CurrentMenu[i]);

                UpdateArrows(CurrentMenu[i]);
            }
            else if (CurrentMenu[i]->MenuItemType == EChessMenuItemType::CustomWidget)
            {
                tempWidget = CurrentMenu[i]->CustomWidget;
            }

            if (tempWidget.IsValid())
            {
                // Set first selection to first valid widget
                if (selectedIndex == -1)
                {
                    selectedIndex = i;
                }

                leftBox->AddSlot()
                .HAlign(HAlign_Left)
                .AutoHeight()
                [
                    tempWidget.ToSharedRef()
                ];
            }
        }
    }

    TSharedPtr<FChessMenuItem> firstMenuItem = CurrentMenu.IsValidIndex(selectedIndex) ? CurrentMenu[selectedIndex] : NULL;
    if (firstMenuItem.IsValid() && firstMenuItem->MenuItemType != EChessMenuItemType::CustomWidget)
    {
        firstMenuItem->SlateWidget->SetMenuItemActive(true);
        FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
    }
}

void SChessMenuWidget::BuildRightPanel()
{
    rightBox->ClearChildren();

    if (NextMenu.Num() == 0)
    {
        return;
    }

    // Build the widgets
    for (int32 i = 0; i != NextMenu.Num(); ++i)
    {
        if (NextMenu[i]->IsVisible)
        {
            TSharedPtr<SChessMenuItem> tempButton;

            if (NextMenu[i]->MenuItemType == EChessMenuItemType::Standard)
            {
                tempButton = SAssignNew(NextMenu[i]->SlateWidget, SChessMenuItem)
                    .playerOwner(this->playerOwner)
                    .text(NextMenu[i]->GetText().ToString())
                    .inactiveTextAlpha(0.3f)
                    .isMultiChoiceMenuItem(false);
            }
            else if (NextMenu[i]->MenuItemType == EChessMenuItemType::MultiChoice)
            {
                tempButton = SAssignNew(NextMenu[i]->SlateWidget, SChessMenuItem)
                    .playerOwner(this->playerOwner)
                    .text(NextMenu[i]->GetText().ToString())
                    .inactiveTextAlpha(0.3f)
                    .isMultiChoiceMenuItem(true)
                    .optionText(this, &SChessMenuWidget::GetOptionText, NextMenu[i]);
            }


            if (tempButton.IsValid())
            {
                rightBox->AddSlot()
                .HAlign(HAlign_Center)
                .AutoHeight()
                [
                    tempButton.ToSharedRef()
                ];
            }
        }
    }
}

void SChessMenuWidget::EnterSubMenu()
{
    isLeftMenuChanging = true;
    isGoingBack = false;
    FSlateApplication::Get().PlaySound(menuStyle->MenuEnterSound, GetOwnerUserIndex());
}

void SChessMenuWidget::MenuGoBack(bool silent /* = false */)
{
    if (MenuHistory.Num() > 0)
    {
        if (!silent)
        {
            FSlateApplication::Get().PlaySound(menuStyle->MenuBackSound, GetOwnerUserIndex());
        }

        isLeftMenuChanging = true;
        isGoingBack = true;
        OnMenuGoBack.ExecuteIfBound(CurrentMenu);
    }
}

void SChessMenuWidget::ConfirmMenuItem()
{
    if (CurrentMenu[selectedIndex]->OnConfirmMenuItem.IsBound())
    {
        CurrentMenu[selectedIndex]->OnConfirmMenuItem.Execute();
    }
    else if (CurrentMenu[selectedIndex]->SubMenu.Num() > 0)
    {
        EnterSubMenu();
    }
}

void SChessMenuWidget::BuildAndShowMenu()
{
    screenRes = GEngine->GetGameUserSettings()->GetScreenResolution();

    // Build left menu panel
    isLeftMenuChanging = false;
    isGoingBack = false;
    BuildLeftPanel(isGoingBack);

    // Setup the whole main menu animations and launch them
    SetupAnimations();

    // Setup the right side and launch animation if there is a submenu
    if (CurrentMenu.Num() > 0 && CurrentMenu.IsValidIndex(selectedIndex) && CurrentMenu[selectedIndex]->IsVisible)
    {
        NextMenu = CurrentMenu[selectedIndex]->SubMenu;
        if (NextMenu.Num() > 0)
        {
            BuildRightPanel();
            isSubmenuChanging = true;
        }
    }

    isMenuHiding = false;
    FSlateApplication::Get().PlaySound(menuStyle->MenuEnterSound, GetOwnerUserIndex());
}

void SChessMenuWidget::HideMenu()
{
    if (!isMenuHiding)
    {
        if (menuWidgetAnimation.IsAtEnd())
        {
            menuWidgetAnimation.PlayReverse(SharedThis(this), false); // TODO: Delete Arguments if it fails
        }
        else
        {
            menuWidgetAnimation.Reverse();
        }

        isMenuHiding = true;
    }
}

void SChessMenuWidget::UpdateArrows(TSharedPtr<FChessMenuItem> menuItem)
{
    const int32 minIndex = menuItem->MinMultiChoiceIndex > -1 ? menuItem->MinMultiChoiceIndex : 0;
    const int32 maxIndex = menuItem->MaxMultiChoiceIndex > -1 ? menuItem->MaxMultiChoiceIndex : menuItem->MultiChoice.Num() - 1;
    const int32 currentIndex = menuItem->SelectedMultiChoiceIndex;

    if (currentIndex > minIndex)
    {
        menuItem->SlateWidget->LeftArrowVisible = EVisibility::Visible;
    }
    else
    {
        menuItem->SlateWidget->LeftArrowVisible = EVisibility::Collapsed;
    }

    if (currentIndex < maxIndex)
    {
        menuItem->SlateWidget->RightArrowVisible = EVisibility::Visible;
    }
    else
    {
        menuItem->SlateWidget->RightArrowVisible = EVisibility::Collapsed;
    }
}

void SChessMenuWidget::ChangeOption(int32 moveBy)
{
    TSharedPtr<FChessMenuItem> menuItem = CurrentMenu[selectedIndex];

    const int32 minIndex = menuItem->MinMultiChoiceIndex > -1 ? menuItem->MinMultiChoiceIndex : 0;
    const int32 maxIndex = menuItem->MaxMultiChoiceIndex > -1 ? menuItem->MaxMultiChoiceIndex : menuItem->MultiChoice.Num() - 1;
    const int32 currentIndex = menuItem->SelectedMultiChoiceIndex;

    if (menuItem->MenuItemType == EChessMenuItemType::MultiChoice)
    {
        if (currentIndex + moveBy >= minIndex && currentIndex + moveBy <= maxIndex)
        {
            menuItem->SelectedMultiChoiceIndex += moveBy;
            menuItem->OnOptionChanged.ExecuteIfBound(menuItem, menuItem->SelectedMultiChoiceIndex);
            FSlateApplication::Get().PlaySound(menuStyle->MenuOptionChangedSound, GetOwnerUserIndex());
        }

        UpdateArrows(menuItem);
    }
}

int32 SChessMenuWidget::GetNextValidIndex(int32 moveBy)
{
    int32 result = selectedIndex;
    if (moveBy != 0 && selectedIndex + moveBy > -1 && selectedIndex + moveBy < CurrentMenu.Num())
    {
        result = selectedIndex + moveBy;

        // Look for non hidden menu item
        while (!CurrentMenu[result]->SlateWidget.IsValid())
        {
            moveBy > 0 ? result++ : result--;

            if (!CurrentMenu.IsValidIndex(result))
            {
                result = selectedIndex;
                break;
            }
        }
    }

    return result;
}

void SChessMenuWidget::LockControls(bool enable)
{
    isControlsLocked = enable;
}

int32 SChessMenuWidget::GetOwnerUserIndex()
{
    return playerOwner.IsValid() ? playerOwner->GetControllerId() : 0;
}

int32 SChessMenuWidget::GetMenuLevel()
{
    return MenuHistory.Num();
}

void SChessMenuWidget::FadeIn()
{
    // Start the menu widget playing animation
    menuWidgetAnimation.Play(SharedThis(this), false); // TODO: Delete Arguments if it fails

    // Enter UI mode
    FSlateApplication::Get().SetKeyboardFocus(SharedThis(this));
}

EVisibility SChessMenuWidget::GetSlateVisibility() const
{
    return isConsoleVisible ? EVisibility::HitTestInvisible : EVisibility::Visible;
}

FVector2D SChessMenuWidget::GetBottomScale() const
{
    return FVector2D(bottomScaleYCurve.GetLerp(), bottomScaleYCurve.GetLerp());
}

FLinearColor SChessMenuWidget::GetBottomColor() const
{
    return FMath::Lerp(FLinearColor(1, 1, 1, 0), FLinearColor(1, 1, 1, 1), bottomColorCurve.GetLerp());
}

FLinearColor SChessMenuWidget::GetTopColor() const
{
    return FMath::Lerp(FLinearColor(1, 1, 1, 0), FLinearColor(1, 1, 1, 1), topColorCurve.GetLerp());
}

FMargin SChessMenuWidget::GetMenuOffset() const
{
    const float widgetWidth = leftBox->GetDesiredSize().X + rightBox->GetDesiredSize().X;
    const float widgetHeight = leftBox->GetDesiredSize().Y + menuHeaderHeight;
    const float offsetX = (screenRes.X - widgetWidth - outlineWidth * 2) / 2;
    const float animationProgress = buttonsPosXCurve.GetLerp();

    FMargin result;
    switch (animationNumber)
    {
        case 0:
            result = FMargin(offsetX + screenRes.X - animationProgress * screenRes.X, (screenRes.Y - widgetHeight) / 2, 0, 0);
            break;

        case 1:
            result = FMargin(offsetX - screenRes.X + animationProgress * screenRes.X, (screenRes.Y - widgetHeight) / 2, 0, 0);
            break;
        
        case 2:
            result = FMargin(offsetX, (screenRes.Y - widgetHeight) / 2 + screenRes.Y - animationProgress * screenRes.Y, 0, 0);
            break;

        case 3:
            result = FMargin(offsetX, (screenRes.Y - widgetHeight) / 2 + -screenRes.Y + animationProgress * screenRes.Y, 0, 0);
            break;
    }

    return result;
}

FMargin SChessMenuWidget::GetLeftMenuOffset() const
{
    const float leftBoxSizeX = leftBox->GetDesiredSize().X + outlineWidth * 2;
    return FMargin(0, 0, -leftBoxSizeX + leftMenuScrollOutCurve.GetLerp() * leftBoxSizeX, 0);
}

FMargin SChessMenuWidget::GetSubMenuOffset() const
{
    const float rightBoxSizeX = rightBox->GetDesiredSize().X + outlineWidth * 2;
    return FMargin(0, 0, -rightBoxSizeX + subMenuScrollOutCurve.GetLerp() * rightBoxSizeX, 0);
}

FSlateColor SChessMenuWidget::GetHeaderColor() const
{
    return CurrentMenuTitle.IsEmpty() ? FLinearColor::Transparent : FLinearColor::White;
}

FReply SChessMenuWidget::ButtonClicked(int32 buttonIndex)
{
    if (isControlsLocked)
    {
        return FReply::Handled();
    }

    if (selectedIndex != buttonIndex)
    {
        TSharedPtr<SChessMenuItem> menuItem = CurrentMenu[selectedIndex]->SlateWidget;
        menuItem->SetMenuItemActive(false);

        selectedIndex = buttonIndex;
        menuItem = CurrentMenu[selectedIndex]->SlateWidget;
        menuItem->SetMenuItemActive(true);

        NextMenu = CurrentMenu[selectedIndex]->SubMenu;
        isSubmenuChanging = true;
        FSlateApplication::Get().PlaySound(menuStyle->MenuItemSelectionChangedSound, GetOwnerUserIndex());
    }
    else if (selectedIndex == buttonIndex)
    {
        ConfirmMenuItem();
    }

    return FReply::Handled().SetUserFocus(SharedThis(this), EFocusCause::SetDirectly);
}

FString SChessMenuWidget::GetOptionText(TSharedPtr<FChessMenuItem> menuItem) const
{
    FString result;

    if (menuItem->SelectedMultiChoiceIndex > -1 && menuItem->SelectedMultiChoiceIndex < menuItem->MultiChoice.Num())
    {
        result = menuItem->MultiChoice[menuItem->SelectedMultiChoiceIndex].ToString();
    }

    return result;
}

FText SChessMenuWidget::GetMenuTitle() const
{
    return CurrentMenuTitle;
}

#undef LOCTEXT_NAMESPACE