// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessMenuSoundsWidgetStyle.h"


FChessMenuSoundsStyle::FChessMenuSoundsStyle()
{
}

FChessMenuSoundsStyle::~FChessMenuSoundsStyle()
{
}

const FName FChessMenuSoundsStyle::TypeName(TEXT("FChessMenuSoundsStyle"));

const FChessMenuSoundsStyle& FChessMenuSoundsStyle::GetDefault()
{
	static FChessMenuSoundsStyle Default;
	return Default;
}

void FChessMenuSoundsStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
}

