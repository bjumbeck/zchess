// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ChessMenuWidgetStyle.generated.h"

/// Represents the appearance of the SChessMenuWidget
USTRUCT()
struct ZCHESS_API FChessMenuStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FChessMenuStyle();
	virtual ~FChessMenuStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FChessMenuStyle& GetDefault();

	/// The Style for the Title text
	UPROPERTY(EditAnywhere, Category = Appearance)
	FTextBlockStyle TitleTextStyle;

	/// The Image used for the main menu background
	UPROPERTY(EditAnywhere, Category = Appearance)
	FSlateBrush MainMenuBackgroundImageBrush;

    /// The brush used for the Title Header's background
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush HeaderBackgroundBrush;
    FChessMenuStyle& SetHeaderBackgroundBrush(const FSlateBrush& inHeaderBackgroundBrush) { HeaderBackgroundBrush = inHeaderBackgroundBrush; return *this; }

    /// The brush used for the left side of the menu
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush LeftBackgroundBrush;
    FChessMenuStyle& SetLeftBackgroundBrush(const FSlateBrush& inLeftBackgroundBrush) { LeftBackgroundBrush = inLeftBackgroundBrush; return *this; }

    /// The brush used for the right side of the menu (The sub menus)
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush RightBackgroundBrush;
    FChessMenuStyle& SetRightBackgroundBrush(const FSlateBrush& inRightBackgroundBrush) { RightBackgroundBrush = inRightBackgroundBrush; return *this; }

    /// The sound that should play when entering a sub menu
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound MenuEnterSound;
    FChessMenuStyle& SetMenuEnterSound(const FSlateSound& inMenuEnterSound) { MenuEnterSound = inMenuEnterSound; return *this; }

    /// The sound that should play when leaving a sub menu
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound MenuBackSound;
    FChessMenuStyle& SetMenuBackSound(const FSlateSound& inMenuBackSound) { MenuBackSound = inMenuBackSound; return *this; }

    /// The sound that should play when changing a menu item option
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound MenuOptionChangedSound;
    FChessMenuStyle& SetMenuOptionChangedSound(const FSlateSound& inMenuOptionChangedSound) { MenuOptionChangedSound = inMenuOptionChangedSound; return *this; }

    /// The sound that should play when changing the selected menu item
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound MenuItemSelectionChangedSound;
    FChessMenuStyle& SetMenuItemSelectionChangedSound(const FSlateSound& inMenuItemSelectionChangedSound) { MenuItemSelectionChangedSound = inMenuItemSelectionChangedSound; return *this; }
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UChessMenuWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FChessMenuStyle MenuStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
        return static_cast< const struct FSlateWidgetStyle* >(&MenuStyle);
	}
};
