// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SlateBasics.h"
#include "SlateExtras.h"

class ZCHESS_API FChessStyle
{
    public:
        static void Initialize();
        static void Shutdown();

        static const ISlateStyle& Get();
        static FName GetStyleSetName();

        /// Reloads the textures used by our slate renderer
        static void ReloadTextures();

    private:
        static TSharedRef<class FSlateStyleSet> Create();

    private:
        static TSharedPtr<class FSlateStyleSet> chessStyleInstance;
};
