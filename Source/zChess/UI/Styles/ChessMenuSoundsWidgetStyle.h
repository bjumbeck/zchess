// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ChessMenuSoundsWidgetStyle.generated.h"

/// Represents the common menu sounds used in the game
USTRUCT()
struct ZCHESS_API FChessMenuSoundsStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FChessMenuSoundsStyle();
	virtual ~FChessMenuSoundsStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FChessMenuSoundsStyle& GetDefault();

    /// The sound that should play when starting the game
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound StartGameSound;
    FChessMenuSoundsStyle& SetStartGameSound(const FSlateSound& inStartGameSound) { StartGameSound = inStartGameSound; return *this; }

    /// The sound that should play when exiting the game
    UPROPERTY(EditAnywhere, Category = Sound)
    FSlateSound ExitGameSound;
    FChessMenuSoundsStyle& SetExitGameSound(const FSlateSound& inExitGameSound) { ExitGameSound = inExitGameSound; return *this; }
};

/**
 */
UCLASS(hidecategories=Object, MinimalAPI)
class UChessMenuSoundsWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

public:
	/** The actual data describing the widget appearance. */
	UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	FChessMenuSoundsStyle MenuSoundsStyle;

	virtual const struct FSlateWidgetStyle* const GetStyle() const override
	{
        return static_cast< const struct FSlateWidgetStyle* >(&MenuSoundsStyle);
	}
};
