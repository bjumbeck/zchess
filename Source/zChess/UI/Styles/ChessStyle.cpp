// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessStyle.h"
#include "SlateGameResources.h"

TSharedPtr<FSlateStyleSet> FChessStyle::chessStyleInstance = NULL;

void FChessStyle::Initialize()
{
    if (!chessStyleInstance.IsValid())
    {
        chessStyleInstance = Create();
        FSlateStyleRegistry::RegisterSlateStyle(*chessStyleInstance);
    }
}

void FChessStyle::Shutdown()
{
    FSlateStyleRegistry::UnRegisterSlateStyle(*chessStyleInstance);
    ensure(chessStyleInstance.IsUnique());
    chessStyleInstance.Reset();
}

const ISlateStyle& FChessStyle::Get()
{
    return *chessStyleInstance;
}

FName FChessStyle::GetStyleSetName()
{
    static FName styleSetName(TEXT("ChessStyle"));
    return styleSetName;
}

void FChessStyle::ReloadTextures()
{
    FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
}

#define IMAGE_BRUSH(RelativePath, ...) FSlateImageBrush(FPaths::GameContentDir() / "Slate" / RelativePath + TEXT(".png"), __VA_ARGS__)
#define BOX_BRUSH(RelativePath, ...) FSlateBoxBrush(FPaths::GameContentDir() / "Slate" / RelativePath + TEXT(".png"), __VA_ARGS__)
#define BORDER_BRUSH(RelativePath, ...) FSlateBorderBrush(FPaths::GameContentDir() / "Slate" / RelativePath + TEXT(".png"), __VA_ARGS__)
#define TTF_FONT( RelativePath, ... ) FSlateFontInfo( FPaths::EngineContentDir() / "Slate"/ RelativePath + TEXT(".ttf"), __VA_ARGS__ )
#define OTF_FONT( RelativePath, ... ) FSlateFontInfo( FPaths::GameContentDir() / "Slate"/ RelativePath + TEXT(".otf"), __VA_ARGS__ )

TSharedRef<FSlateStyleSet> FChessStyle::Create()
{
    TSharedRef<FSlateStyleSet> styleRef = FSlateGameResources::New(FChessStyle::GetStyleSetName(), "/Game/UI/Styles", "/Game/UI/Styles");
    FSlateStyleSet& style = styleRef.Get();

    // Define any fonts we want to use below
	style.Set("ChessGame.MenuServerListTextStyle", FTextBlockStyle()
		.SetFont(TTF_FONT("Fonts/Roboto-Black", 14))
		.SetColorAndOpacity(FLinearColor::White)
		.SetShadowOffset(FIntPoint(-1, 1))
	);

	style.Set("ChessGame.MenuTextStyle", FTextBlockStyle()
		.SetFont(TTF_FONT("Fonts/Roboto-Black", 18))
		.SetColorAndOpacity(FLinearColor::White)
		.SetShadowOffset(FIntPoint(-1, 1))
	);

	style.Set("ChessGame.MenuHeaderTextStyle", FTextBlockStyle()
		.SetFont(TTF_FONT("Fonts/Roboto-Black", 26))
		.SetColorAndOpacity(FLinearColor::White)
		.SetShadowOffset(FIntPoint(-1, 1))
	);
    
    return styleRef;
}

#undef IMAGE_BRUSH
#undef BOX_BRUSH
#undef BORDER_BRUSH
#undef TTF_FONT
#undef OTF_FONT