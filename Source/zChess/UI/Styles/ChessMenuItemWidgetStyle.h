// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Styling/SlateWidgetStyle.h"
#include "SlateWidgetStyleContainerBase.h"

#include "ChessMenuItemWidgetStyle.generated.h"


/// Represents the appearance of an FChessMenuItem
USTRUCT()
struct ZCHESS_API FChessMenuItemStyle : public FSlateWidgetStyle
{
	GENERATED_USTRUCT_BODY()

	FChessMenuItemStyle();
	virtual ~FChessMenuItemStyle();

	// FSlateWidgetStyle
	virtual void GetResources(TArray<const FSlateBrush*>& OutBrushes) const override;
	static const FName TypeName;
	virtual const FName GetTypeName() const override { return TypeName; };
	static const FChessMenuItemStyle& GetDefault();

	/// The Style for the main text
	UPROPERTY(EditAnywhere, Category = Appearance)
	FTextBlockStyle MainTextStyle;

	/// The Style for the option text
	UPROPERTY(EditAnywhere, Category = Appearance)
	FTextBlockStyle OptionTextStyle;

    /// The brush used for the menu item background
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush BackgroundBrush;
    FChessMenuItemStyle& SetBackgroundBrush(const FSlateBrush& inBackgroundBrush) { BackgroundBrush = inBackgroundBrush; return *this; }

    /// The brush used for the left arrow on the menu item
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush LeftArrowImage;
    FChessMenuItemStyle& SetLeftArrowImage(const FSlateBrush& inLeftArrowImage) { LeftArrowImage = inLeftArrowImage; return *this; }

    /// The brush used for the right arrow on the menu item
    UPROPERTY(EditAnywhere, Category = Appearance)
    FSlateBrush RightArrowImage;
    FChessMenuItemStyle& SetRightArrowImage(const FSlateBrush& inRightArrowImage) { RightArrowImage = inRightArrowImage; return *this; }
};

UCLASS(hidecategories=Object, MinimalAPI)
class UChessMenuItemWidgetStyle : public USlateWidgetStyleContainerBase
{
	GENERATED_BODY()

    public:
	    /** The actual data describing the widget appearance. */
	    UPROPERTY(Category=Appearance, EditAnywhere, meta=(ShowOnlyInnerProperties))
	    FChessMenuItemStyle MenuItemStyle;

	    virtual const struct FSlateWidgetStyle* const GetStyle() const override
	    {
            return static_cast< const struct FSlateWidgetStyle* >(&MenuItemStyle);
	    }
};
