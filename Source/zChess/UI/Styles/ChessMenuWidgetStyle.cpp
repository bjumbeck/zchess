// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessMenuWidgetStyle.h"


FChessMenuStyle::FChessMenuStyle()
{
}

FChessMenuStyle::~FChessMenuStyle()
{
}

const FName FChessMenuStyle::TypeName(TEXT("FChessMenuStyle"));

const FChessMenuStyle& FChessMenuStyle::GetDefault()
{
	static FChessMenuStyle Default;
	return Default;
}

void FChessMenuStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
	OutBrushes.Add(&HeaderBackgroundBrush);
    OutBrushes.Add(&LeftBackgroundBrush);
    OutBrushes.Add(&RightBackgroundBrush);
}

