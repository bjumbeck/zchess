// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessMenuItemWidgetStyle.h"


FChessMenuItemStyle::FChessMenuItemStyle()
{
}

FChessMenuItemStyle::~FChessMenuItemStyle()
{
}

const FName FChessMenuItemStyle::TypeName(TEXT("FChessMenuItemStyle"));

const FChessMenuItemStyle& FChessMenuItemStyle::GetDefault()
{
	static FChessMenuItemStyle Default;
	return Default;
}

void FChessMenuItemStyle::GetResources(TArray<const FSlateBrush*>& OutBrushes) const
{
	// Add any brush resources here so that Slate can correctly atlas and reference them
    OutBrushes.Add(&BackgroundBrush);
    OutBrushes.Add(&LeftArrowImage);
    OutBrushes.Add(&RightArrowImage);
}

