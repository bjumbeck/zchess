// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "OnlineSessionInterface.h"
#include "OnlineIdentityInterface.h"
#include "UI/Menu/ChessMainMenu.h"
#include "ChessGameInstance.generated.h"

namespace ChessGameInstanceState
{
    extern const FName None;
    extern const FName MainMenu;
    extern const FName Playing;
}

UCLASS(config = Game)
class ZCHESS_API UChessGameInstance : public UGameInstance
{
    GENERATED_BODY()

    public:
        UChessGameInstance(const FObjectInitializer& objectInitializer);

        // Initialization and Shutdown Methods
        virtual void Init() override;
        virtual void Shutdown() override;
        virtual void StartGameInstance() override;

        // Tick hook that will be used to manage our state changes
        bool Tick(float DeltaSeconds);

        // Online Game Methods
        bool HostGame(ULocalPlayer* localPlayer, const FString& gameType, const FString& travelURL);
        bool JoinSession(ULocalPlayer* localPlayer, int32 sessionIndexInSearchResults);
        bool JoinSession(ULocalPlayer* localPlayer, const FOnlineSessionSearchResult& searchResult);
        bool FindSessions(ULocalPlayer* playerOwner, bool findLAN);
        void TravelToSession(const FName& sessionName);
        void CleanupSessionOnReturnToMenu();
        class AChessGameSession* GetGameSession() const;

        // Public State Transition Methods
        void GotoState(FName newState);

        // Start task to get user privilege
        void StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& delegate, EUserPrivileges::Type privilege, TSharedPtr<FUniqueNetId> userId);
        void CleanupOnlinePrivilegeTask();

    private:
        // Used to determine if we have changed game states.
        void TestForStateChange();

        // Internal state management, these will be used to destroy/begin states properly when it is needed.
        // All they do basically is determine what the current state is and then call that states begin or end methods.
        void EndCurrentState();
        void BeginNewState(FName newState, FName prevState);

        // Implementations on what to do when you want a certain state to begin
        void BeginMainMenuState();
        void BeginPlayingState();

        // Implementations on what to do when you want a certain state to end
        void EndMainMenuState();
        void EndPlayingState();

        // Delegates mainly used to handle the loading screen functionality
        void OnPreLoadMap();
        void OnPostLoadMap();
		bool LoadFrontEndMap(const FString& mapName);

        // Callbacks
        void OnEndSessionComplete(FName sessionName, bool wasSuccessful);
        void FinishSessionCreation(EOnJoinSessionCompleteResult::Type result);
        void FinishJoinSession(EOnJoinSessionCompleteResult::Type result);
        void OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type result);
        void OnCreatePresenceSessionComplete(FName sessionName, bool wasSuccessful);
        void OnSearchSessionsComplete(bool wasSuccessful);

        // Internal functions
        void SetPresenceForLocalPlayers(const FVariantData& presenceData);
        void InternalTravelToSession(const FName& sessionName);

    private:
		UPROPERTY(config)
		FString mainMenuMap;

        /// Variables to track the state of the game instance
        FName currentState;
        FName pendingState;

        /// URL to travel to after pending network operations
        FString travelURL;

		/// Main Menu UI
		TSharedPtr<FChessMainMenu> mainMenuUI;

        /// Delegate for to hook into Tick()
        FTickerDelegate tickDelegate;

        /// Delegate for ending a session
        FOnEndSessionCompleteDelegate onEndSessionCompleteDelegate;

        /// Handles to various registered delegates
        FDelegateHandle tickDelegateHandle;                                 // Keep
        FDelegateHandle onJoinSessionCompleteDelegateHandle;                // Keep
        FDelegateHandle onSearchSessionsCompleteDelegateHandle;             // Keep
        FDelegateHandle onStartSessionCompleteDelegateHandle;               // Keep
        FDelegateHandle onEndSessionCompleteDelegateHandle;                 // Keep
        FDelegateHandle onDestroySessionCompleteDelegateHandle;             // Keep
        FDelegateHandle onCreatePresenceSessionCompleteDelegateHandle;      // Keep
};
