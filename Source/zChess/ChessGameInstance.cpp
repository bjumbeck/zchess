// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "Online/ChessGameSession.h"
#include "Player/ChessPlayerState.h"
#include "Controllers/ChessPlayerController.h"
#include "ChessGameInstance.h"
#include "OnlineSubsystem.h"
#include "Engine.h"

namespace ChessGameInstanceState
{
    const FName None = FName(TEXT("None"));
    const FName MainMenu = FName(TEXT("MainMenu"));
    const FName Playing = FName(TEXT("Playing"));
}

UChessGameInstance::UChessGameInstance(const FObjectInitializer& objectInitializer)
    : Super(objectInitializer)
{
    currentState = ChessGameInstanceState::None;
}

void UChessGameInstance::Init()
{
    Super::Init();

    // Game requires the ability to Id users
    IOnlineSubsystem* const onlineSub = IOnlineSubsystem::Get();
    check(onlineSub);

    const IOnlineIdentityPtr identityInterface = onlineSub->GetIdentityInterface();
    check(identityInterface.IsValid());

    const IOnlineSessionPtr sessionInterface = onlineSub->GetSessionInterface();
    check(sessionInterface.IsValid());

    FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UChessGameInstance::OnPreLoadMap);
    FCoreUObjectDelegates::PostLoadMap.AddUObject(this, &UChessGameInstance::OnPostLoadMap); // Get rid of loading screen

    onEndSessionCompleteDelegate = FOnEndSessionCompleteDelegate::CreateUObject(this, &UChessGameInstance::OnEndSessionComplete);

    // Register delegate for ticker callback
    tickDelegate = FTickerDelegate::CreateUObject(this, &UChessGameInstance::Tick);
    tickDelegateHandle = FTicker::GetCoreTicker().AddTicker(tickDelegate);
}

void UChessGameInstance::Shutdown()
{
    Super::Shutdown();

    // Unregister ticker
    FTicker::GetCoreTicker().RemoveTicker(tickDelegateHandle);

    // Cleanup Session
    CleanupSessionOnReturnToMenu();
}

void UChessGameInstance::StartGameInstance()
{
    Super::StartGameInstance();

    GotoState(ChessGameInstanceState::MainMenu);
}

bool UChessGameInstance::Tick(float DeltaSeconds)
{  
    // Check to see if we need to change state (If pending state != current state) and if so change it.
    TestForStateChange();

    return true;
}

bool UChessGameInstance::HostGame(ULocalPlayer* localPlayer, const FString& gameType, const FString& travelURL)
{
    // Online Game
    //////////////////////////////////////////////////////////////////////////
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        // Add callback for completion
        onCreatePresenceSessionCompleteDelegateHandle = gameSession->OnCreatePresenceSessionComplete().AddUObject(this, &UChessGameInstance::OnCreatePresenceSessionComplete);

        this->travelURL = travelURL;
        bool const isLanMatch = travelURL.Contains(TEXT("?bIsLanMatch")); // TODO: Is the bIsLanMatch correct?

        // Determine map name from URL
        const FString& mapNameSubStr = "/Game/Maps/";
        const FString& choppedMapName = travelURL.RightChop(mapNameSubStr.Len());
        // TODO: Need to find the first / here and return a right chop of everything after it?
        const FString& mapName = choppedMapName.LeftChop(choppedMapName.Len() - choppedMapName.Find("?game"));

        if (gameSession->HostSession(localPlayer->GetPreferredUniqueNetId(), GameSessionName, gameType, mapName, isLanMatch, true, AChessGameSession::DEFAULT_NUM_PLAYERS))
        {
            // If any error occurred above, pending state would be set
            if (pendingState == currentState || pendingState == ChessGameInstanceState::None)
            {
                GotoState(ChessGameInstanceState::Playing);
                return true;
            }
        }
    }

    return false;
}

bool UChessGameInstance::JoinSession(ULocalPlayer* localPlayer, int32 sessionIndexInSearchResults)
{
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        // Add callback for when the join request is complete
        onJoinSessionCompleteDelegateHandle = gameSession->OnJoinSessionComplete().AddUObject(this, &UChessGameInstance::OnJoinSessionComplete);
        if (gameSession->JoinSession(localPlayer->GetPreferredUniqueNetId(), GameSessionName, sessionIndexInSearchResults))
        {
            // If any error occurred above, pending state would be set
            if (pendingState == currentState || pendingState == ChessGameInstanceState::None)
            {
                GotoState(ChessGameInstanceState::Playing);
                return true;
            }
        }
    }

    return false;
}

bool UChessGameInstance::JoinSession(ULocalPlayer* localPlayer, const FOnlineSessionSearchResult& searchResult)
{
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        onJoinSessionCompleteDelegateHandle = gameSession->OnJoinSessionComplete().AddUObject(this, &UChessGameInstance::OnJoinSessionComplete);
        if (gameSession->JoinSession(localPlayer->GetPreferredUniqueNetId(), GameSessionName, searchResult))
        {
            // If any error occurred above, pending state would be set
            if (pendingState == currentState || pendingState == ChessGameInstanceState::None)
            {
                GotoState(ChessGameInstanceState::Playing);
                return true;
            }
        }
    }

    return false;
}

bool UChessGameInstance::FindSessions(ULocalPlayer* playerOwner, bool findLAN)
{
    bool result = false;

    check(playerOwner != nullptr);
    if (playerOwner)
    {
        AChessGameSession* const gameSession = GetGameSession();
        if (gameSession)
        {
            gameSession->OnFindSessionsComplete().RemoveAll(this);
            onSearchSessionsCompleteDelegateHandle = gameSession->OnFindSessionsComplete().AddUObject(this, &UChessGameInstance::OnSearchSessionsComplete);

            gameSession->FindSessions(playerOwner->GetPreferredUniqueNetId(), GameSessionName, findLAN, true);
            result = true;
        }
    }

    return result;
}

void UChessGameInstance::TravelToSession(const FName& sessionName)
{
    GotoState(ChessGameInstanceState::Playing);
    InternalTravelToSession(sessionName);
}

void UChessGameInstance::CleanupSessionOnReturnToMenu()
{
    // End online game and destroy it
    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    IOnlineSessionPtr sessions = (onlineSub != NULL) ? onlineSub->GetSessionInterface() : NULL;

    if (sessions.IsValid())
    {
        EOnlineSessionState::Type sessionState = sessions->GetSessionState(GameSessionName);
        UE_LOG(LogOnline, Log, TEXT("Session %s is '%s'"), *GameSessionName.ToString(), EOnlineSessionState::ToString(sessionState));

        if (sessionState == EOnlineSessionState::InProgress)
        {
            UE_LOG(LogOnline, Log, TEXT("Ending session %s on return to main menu"), *GameSessionName.ToString());
            onEndSessionCompleteDelegateHandle = sessions->AddOnEndSessionCompleteDelegate_Handle(onEndSessionCompleteDelegate);
            sessions->EndSession(GameSessionName);
        }
        else if (sessionState == EOnlineSessionState::Ending)
        {
            UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to  end on return to main menu"), *GameSessionName.ToString());
            onEndSessionCompleteDelegateHandle = sessions->AddOnEndSessionCompleteDelegate_Handle(onEndSessionCompleteDelegate);
        }
        else if (sessionState == EOnlineSessionState::Ended || sessionState == EOnlineSessionState::Pending)
        {
            UE_LOG(LogOnline, Log, TEXT("Destroying session %s on return to main menu"), *GameSessionName.ToString());
            onDestroySessionCompleteDelegateHandle = sessions->AddOnDestroySessionCompleteDelegate_Handle(onEndSessionCompleteDelegate);
            sessions->DestroySession(GameSessionName);
        }
        else if (sessionState == EOnlineSessionState::Starting)
        {
            UE_LOG(LogOnline, Log, TEXT("Waiting for session %s to start, and then we will end it to return to main menu"), *GameSessionName.ToString());
            onStartSessionCompleteDelegateHandle = sessions->AddOnStartSessionCompleteDelegate_Handle(onEndSessionCompleteDelegate);
        }
    }
}

AChessGameSession* UChessGameInstance::GetGameSession() const
{
    UWorld* const world = GetWorld();
    if (world)
    {
        AGameMode* const game = world->GetAuthGameMode();
        if (game)
        {
            return Cast<AChessGameSession>(game->GameSession);
        }
    }

    return nullptr;
}

void UChessGameInstance::GotoState(FName newState)
{
    UE_LOG(LogOnline, Log, TEXT("GotoState: NewState: %s"), *newState.ToString());
    pendingState = newState;
}

void UChessGameInstance::StartOnlinePrivilegeTask(const IOnlineIdentity::FOnGetUserPrivilegeCompleteDelegate& delegate, EUserPrivileges::Type privilege, TSharedPtr<FUniqueNetId> userId)
{
    // TODO: We could use a pop up message here letting them know what we are doing or something

    auto identity = Online::GetIdentityInterface();
    if (identity.IsValid() && userId.IsValid())
    {
        identity->GetUserPrivilege(*userId, privilege, delegate);
    }
    else
    {
        // We can fake it here
        delegate.ExecuteIfBound(FUniqueNetIdString(), privilege, StaticCast<uint32>(IOnlineIdentity::EPrivilegeResults::NoFailures));
    }
}

void UChessGameInstance::CleanupOnlinePrivilegeTask()
{
   // This is just here incase we displayed a warning or notice when starting the online privilege check
}

void UChessGameInstance::TestForStateChange()
{
    if (pendingState != currentState && pendingState != ChessGameInstanceState::None)
    {
        const FName oldState = currentState;

        EndCurrentState();
        BeginNewState(pendingState, oldState);
        pendingState = ChessGameInstanceState::None;
    }
}

void UChessGameInstance::EndCurrentState()
{
    if (currentState == ChessGameInstanceState::MainMenu)
    {
        EndMainMenuState();
    }
    else if (currentState == ChessGameInstanceState::Playing)
    {
        EndPlayingState();
    }

    currentState = ChessGameInstanceState::None;
}

void UChessGameInstance::BeginNewState(FName newState, FName prevState)
{
    if (newState == ChessGameInstanceState::MainMenu)
    {
        BeginMainMenuState();
    }
    else if (newState == ChessGameInstanceState::Playing)
    {
        BeginPlayingState();
    }

    currentState = newState;
}

void UChessGameInstance::BeginMainMenuState()
{
	// Don't allow split screen (Though we don't support it anyways)
	GetGameViewportClient()->SetDisableSplitscreenOverride(true);

	// Set presence t o menu state for the owning player
	SetPresenceForLocalPlayers(FVariantData(FString(TEXT("OnMenu"))));

	// Load the startup map
	LoadFrontEndMap(TEXT("Game/Maps/MainMenu"));

	// Player 0 gets to own the UI
	ULocalPlayer* const player = GetFirstGamePlayer();

	mainMenuUI = MakeShareable(new FChessMainMenu());
	mainMenuUI->Construct(this, player);
	mainMenuUI->AddMenuToGameViewport();
}

void UChessGameInstance::BeginPlayingState()
{
    SetPresenceForLocalPlayers(FVariantData(FString(TEXT("InGame"))));
}

void UChessGameInstance::EndMainMenuState()
{
	if (mainMenuUI.IsValid())
	{
		mainMenuUI->RemoveMenuFromGameViewport();
		mainMenuUI = nullptr;
	}
}

void UChessGameInstance::EndPlayingState()
{
    SetPresenceForLocalPlayers(FVariantData(FString(TEXT("OnMenu"))));

    const UWorld* world = GetWorld();
    // TODO: Finish after create the AChessGameState class
    //     const AChessGameState* gameState = world != NULL ? world->GetGameState<AChessGameState>() : NULL;
    //     if (gameState)
    //     {
    //         // Send round end events for local player(s)
    //         for (int i = 0; i < LocalPlayers.Num(); ++i)
    //         {
    //             AChessPlayerController* chessPC = Cast<AChessPlayerController>(LocalPlayers[i]->PlayerController);
    //             if (chessPC)
    //             {
    //                 chessPC->ClientSendRoundEndEvent(false, gameState->ElapsedTime);
    //             }
    //         }
    // 
    //         gameState->RequestFinishAndExitToMainMenu();
    //     }
    //     else
    //     {
    //         CleanupSessionOnReturnToMenu();
    //     }
    CleanupSessionOnReturnToMenu();
}

void UChessGameInstance::OnPreLoadMap()
{
}

void UChessGameInstance::OnPostLoadMap()
{
    // TODO: Once we implement the loading screen class make sure we hide the loading screen when the level is done loading.
}

bool UChessGameInstance::LoadFrontEndMap(const FString& mapName)
{
	bool success = true;

	// If already loaded do nothing
	UWorld* const world = GetWorld();
	if (world)
	{
		const FString currentMapName = *world->PersistentLevel->GetOutermost()->GetName();
		if (currentMapName == mapName)
		{
			return success;
		}
	}

	FString error;
	EBrowseReturnVal::Type browseReturn = EBrowseReturnVal::Failure;
	FURL url(*FString::Printf(TEXT("%s"), *mapName));

	if (url.Valid && !HasAnyFlags(RF_ClassDefaultObject))
	{
		browseReturn = GetEngine()->Browse(*WorldContext, url, error);

		// Handle failure
		if (browseReturn != EBrowseReturnVal::Success)
		{
			UE_LOG(LogLoad, Fatal, TEXT("%s"), *FString::Printf(TEXT("Failed to enter %s: %s. Please check the log for errors."), *mapName, *error));
			success = false;
		}
	}

	return success;
}

void UChessGameInstance::OnEndSessionComplete(FName sessionName, bool wasSuccessful)
{
    UE_LOG(LogOnline, Log, TEXT("UChessGameInstance::OnEndSessionComplete: Session = %s WasSuccessful = %s"), *sessionName.ToString(), wasSuccessful ? TEXT("True") : TEXT("False"));

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            sessions->ClearOnStartSessionCompleteDelegate_Handle(onStartSessionCompleteDelegateHandle);
            sessions->ClearOnEndSessionCompleteDelegate_Handle(onEndSessionCompleteDelegateHandle);
            sessions->ClearOnDestroySessionCompleteDelegate_Handle(onDestroySessionCompleteDelegateHandle);
        }
    }

    // Continue
    CleanupSessionOnReturnToMenu();
}

void UChessGameInstance::FinishSessionCreation(EOnJoinSessionCompleteResult::Type result)
{
    if (result == EOnJoinSessionCompleteResult::Success)
    {
        // Travel to the specified match URL
        GetWorld()->ServerTravel(travelURL);
    }
    else
    {
        // TODO: Implement a way to show why we are returning to main menu
        GotoState(ChessGameInstanceState::MainMenu);
    }
}

void UChessGameInstance::FinishJoinSession(EOnJoinSessionCompleteResult::Type result)
{
    if (result != EOnJoinSessionCompleteResult::Success)
    {
        // TODO: Implement a way to show why we are returning to main menu
        GotoState(ChessGameInstanceState::MainMenu);
        return;
    }

    InternalTravelToSession(GameSessionName);
}

void UChessGameInstance::OnJoinSessionComplete(EOnJoinSessionCompleteResult::Type result)
{
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        gameSession->OnJoinSessionComplete().Remove(onJoinSessionCompleteDelegateHandle);
    }

    FinishJoinSession(result);
}

void UChessGameInstance::OnCreatePresenceSessionComplete(FName sessionName, bool wasSuccessful)
{
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        gameSession->OnCreatePresenceSessionComplete().Remove(onCreatePresenceSessionCompleteDelegateHandle);
        FinishSessionCreation(wasSuccessful ? EOnJoinSessionCompleteResult::Success : EOnJoinSessionCompleteResult::UnknownError);
    }
}

void UChessGameInstance::OnSearchSessionsComplete(bool wasSuccessful)
{
    AChessGameSession* const gameSession = GetGameSession();
    if (gameSession)
    {
        gameSession->OnFindSessionsComplete().Remove(onSearchSessionsCompleteDelegateHandle);
    }
}

void UChessGameInstance::SetPresenceForLocalPlayers(const FVariantData& presenceData)
{
    const auto presence = Online::GetPresenceInterface();
    if (presence.IsValid())
    {
        for (int i = 0; i < LocalPlayers.Num(); ++i)
        {
            const TSharedPtr<FUniqueNetId> userId = LocalPlayers[i]->GetPreferredUniqueNetId();

            if (userId.IsValid())
            {
                FOnlineUserPresenceStatus presenceStatus;
                presenceStatus.Properties.Add(DefaultPresenceKey, presenceData);

                presence->SetPresence(*userId, presenceStatus);
            }
        }
    }
}

void UChessGameInstance::InternalTravelToSession(const FName& sessionName)
{
    APlayerController* const playerController = GetFirstLocalPlayerController();

    if (playerController == nullptr)
    {
        // TODO: Show message here
        GotoState(ChessGameInstanceState::MainMenu);
        return;
    }

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub == nullptr)
    {
        // TODO: Show message here
        GotoState(ChessGameInstanceState::MainMenu);
        return;
    }

    FString url;
    IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
    if (!sessions.IsValid() || !sessions->GetResolvedConnectString(sessionName, url))
    {
        // Show message here
        GotoState(ChessGameInstanceState::MainMenu);
        UE_LOG(LogOnlineGame, Warning, TEXT("Failed to travel to session upon joining it"));
        return;
    }

    playerController->ClientTravel(url, TRAVEL_Absolute);
}