// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ChessBoardActor.generated.h"

UCLASS()
class ZCHESS_API AChessBoardActor : public AActor
{
	GENERATED_BODY()
	
    public:	
	    AChessBoardActor();

	    virtual void BeginPlay() override;
	    virtual void Tick( float DeltaSeconds ) override;

        UFUNCTION()
        void OnBoardClicked(UPrimitiveComponent* component);

        void GetTileComponents(TArray<class UChessTileComponent*>& tilesArray) { tilesArray = tiles; }

    private:
        char ConvertNumberToLetter(int number);

    public:
        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Chessboard)
        UStaticMeshComponent* staticMesh;

        UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Chessboard)
        TArray<class UChessTileComponent*> tiles;
};
