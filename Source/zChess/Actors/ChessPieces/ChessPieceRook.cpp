// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPieceRook.h"

AChessPieceRook::AChessPieceRook()
    : Super()
{
    // Rook Piece Info
    pieceType = EChessPieceType::ROOK;

    // Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_Rook.chess_Rook'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    RootComponent = staticMesh;
}

void AChessPieceRook::BeginPlay()
{
    Super::BeginPlay();

}

void AChessPieceRook::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

void AChessPieceRook::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}