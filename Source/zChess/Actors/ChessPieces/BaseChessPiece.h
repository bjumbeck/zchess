#pragma once

#include "GameFramework/Pawn.h"
#include "BaseChessPiece.generated.h"



/// <summary>Values that represent chess piece colors.</summary>
UENUM(BlueprintType)
enum class EChessPieceColor : uint8
{
    NONE        UMETA(DisplayName = "No Side"),
    BLACK       UMETA(DisplayName = "Black Side"),
    WHITE       UMETA(DisplayName = "White Side")
};

/// <summary>Values that represent chess piece types.</summary>
UENUM(BlueprintType)
enum class EChessPieceType : uint8
{
    NONE        UMETA(DisplayName = "None"),
    KING        UMETA(DisplayName = "King"),
    QUEEN       UMETA(DisplayName = "Queen"),
    ROOK        UMETA(DisplayName = "Rook"),
    BISHOP      UMETA(DisplayName = "Bishop"),
    KNIGHT      UMETA(DisplayName = "Knight"),
    PAWN        UMETA(DisplayName = "Pawn")
};

/// <summary>The base class for all chess piece actors in the game</summary>
UCLASS()
class ZCHESS_API ABaseChessPiece : public APawn
{
	GENERATED_BODY()

    public:
        ABaseChessPiece();

        virtual void BeginPlay() override;
        virtual void Tick(float DeltaSeconds) override;
        virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
        virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent); //override;    This is screwing with the HTML5 build

        UFUNCTION(BlueprintCallable, Category = ChessPiece)
        void SetPieceColor(EChessPieceColor playerSideColor);

        UFUNCTION(BlueprintCallable, Category = ChessPiece)
        EChessPieceColor GetPieceColor() const;

        UFUNCTION(BlueprintCallable, Category = ChessPiece)
        EChessPieceType GetPieceType() const;

    protected:
        UFUNCTION(BlueprintCallable, Category = ChessPiece)
        void SetPieceType(EChessPieceType typeOfPiece);

    protected:
        UMaterial* blackPieceMaterial;
        UMaterial* whitePieceMaterial;

        UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlayerColor)
        EChessPieceColor pieceColor;

        UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PieceType)
        EChessPieceType pieceType;

        UPROPERTY(BlueprintReadWrite, Category = StaticMesh)
        UStaticMeshComponent* staticMesh;
};
