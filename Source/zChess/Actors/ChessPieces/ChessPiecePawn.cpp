// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPiecePawn.h"

AChessPiecePawn::AChessPiecePawn()
    : Super()
{
    // Pawn Piece Info
    pieceType = EChessPieceType::PAWN;

    // Setup Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_Pawn.chess_Pawn'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    RootComponent = staticMesh;
}

// Called when the game starts or when spawned
void AChessPiecePawn::BeginPlay()
{
    Super::BeginPlay();

}

// Called every frame
void AChessPiecePawn::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

// Called to bind functionality to input
void AChessPiecePawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}