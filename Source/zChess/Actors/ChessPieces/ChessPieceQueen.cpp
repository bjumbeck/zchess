// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPieceQueen.h"

AChessPieceQueen::AChessPieceQueen()
    : Super()
{
    // Queen Piece Info
    pieceType = EChessPieceType::QUEEN;

    // Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_Queen.chess_Queen'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    RootComponent = staticMesh;
}

void AChessPieceQueen::BeginPlay()
{
    Super::BeginPlay();

}

void AChessPieceQueen::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

void AChessPieceQueen::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}
