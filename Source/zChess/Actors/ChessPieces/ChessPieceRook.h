// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/ChessPieces/BaseChessPiece.h"
#include "ChessPieceRook.generated.h"

/**
 * 
 */
UCLASS()
class ZCHESS_API AChessPieceRook : public ABaseChessPiece
{
    GENERATED_BODY()
    
    public:
        AChessPieceRook();

        virtual void BeginPlay() override;
        virtual void Tick(float DeltaSeconds) override;
        virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};
