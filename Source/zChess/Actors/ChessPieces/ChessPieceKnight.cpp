// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPieceKnight.h"

AChessPieceKnight::AChessPieceKnight()
    : Super()
{
    // Knight Piece Info
    pieceType = EChessPieceType::KNIGHT;

    // Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_Knight.chess_Knight'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    staticMesh->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
    RootComponent = staticMesh;
}

void AChessPieceKnight::BeginPlay()
{
    Super::BeginPlay();

}

void AChessPieceKnight::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

void AChessPieceKnight::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}