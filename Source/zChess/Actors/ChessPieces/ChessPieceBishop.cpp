// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPieceBishop.h"

/// Default constructor.

AChessPieceBishop::AChessPieceBishop()
    : Super()
{
    // Bishop Piece Info
    pieceType = EChessPieceType::BISHOP;

    // Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_Bishop.chess_Bishop'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    staticMesh->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));
    RootComponent = staticMesh;
}

/// Called when the piece is first spawned.
/// Use this to do any startup behavior that is needed on spawn

void AChessPieceBishop::BeginPlay()
{
    Super::BeginPlay();

}

/// Called once every frame update.
/// Use this function to call any code that needs to update every frame.
/// Stuff like updating variables that track movement, is checking enemy king, etc.
///
/// @param DeltaSeconds The delta time.

void AChessPieceBishop::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

/// Sets up the player input component.
/// Allows a Pawn to set up custom input bindings.
///
/// @param [in,out] InputComponent If non-null, the input component.

void AChessPieceBishop::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}