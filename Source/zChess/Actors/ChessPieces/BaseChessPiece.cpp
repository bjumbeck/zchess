// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "BaseChessPiece.h"

/// <summary>Default constructor.</summary>

ABaseChessPiece::ABaseChessPiece()
    : Super()
{
    static ConstructorHelpers::FObjectFinderOptional<UMaterial> BlackMaterial(TEXT("Material'/Game/Materials/M_ChessPieces_Black.M_ChessPieces_Black'"));
    static ConstructorHelpers::FObjectFinderOptional<UMaterial> WhiteMaterial(TEXT("Material'/Game/Materials/M_ChessPieces_White.M_ChessPieces_White'"));

    PrimaryActorTick.bCanEverTick = true;

    staticMesh = nullptr;

    pieceColor = EChessPieceColor::NONE;
    pieceType = EChessPieceType::NONE;

    blackPieceMaterial = BlackMaterial.Get();
    whitePieceMaterial = WhiteMaterial.Get();
}

/// <summary>Called when the actor is spawned</summary>
void ABaseChessPiece::BeginPlay()
{
	Super::BeginPlay();
	
}

#pragma region Documentation
/// <summary>Ticks.</summary>
///
/// <param name="float DeltaTime">The delta time.</param>
#pragma endregion

void ABaseChessPiece::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

/// Sets up the player input component.
/// Allows you to override input mapping for this actor
///
/// @param [in,out] UInputComponet InputComponent If non-null, the input component.

void ABaseChessPiece::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

// Need this for active updates of the materials used for the color of the piece when changed in editor
void ABaseChessPiece::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
    // This is screwing with the HTML5 build so commenting it out for now.
    // Super::PostEditChangeProperty(PropertyChangedEvent);

    if (PropertyChangedEvent.Property->GetNameCPP() == "pieceColor")
    {
        SetPieceColor(pieceColor);
    }
}

/// Sets piece's materials to the specified color
///
/// @param playerSideColor The piece color to use

void ABaseChessPiece::SetPieceColor(EChessPieceColor playerSideColor)
{
    switch (playerSideColor)
    {
        case EChessPieceColor::BLACK:
            staticMesh->SetMaterial(0, blackPieceMaterial);
            staticMesh->SetMaterial(1, blackPieceMaterial);
            staticMesh->SetMaterial(2, blackPieceMaterial);
            break;

        case EChessPieceColor::WHITE:
            staticMesh->SetMaterial(0, whitePieceMaterial);
            staticMesh->SetMaterial(1, whitePieceMaterial);
            staticMesh->SetMaterial(2, whitePieceMaterial);
            break;

        case EChessPieceColor::NONE:
            break;
    }

    pieceColor = playerSideColor;
}

/// Gets piece color.
///
/// @return The piece color.

EChessPieceColor ABaseChessPiece::GetPieceColor() const
{
    return pieceColor;
}

/// Gets piece type.
///
/// @return The piece type.

EChessPieceType ABaseChessPiece::GetPieceType() const
{
    return pieceType;
}

/// Sets piece type.
///
/// @param typeOfPiece Type of the piece.

void ABaseChessPiece::SetPieceType(EChessPieceType typeOfPiece)
{
    pieceType = typeOfPiece;
}