// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPieceKing.h"

AChessPieceKing::AChessPieceKing()
    : Super()
{
    // King Piece Info
    pieceType = EChessPieceType::KING;

    // Static Mesh
    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/ChessPieces/chess_King.chess_King'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));
    RootComponent = staticMesh;
}

void AChessPieceKing::BeginPlay()
{
    Super::BeginPlay();

}

void AChessPieceKing::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

}

void AChessPieceKing::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    Super::SetupPlayerInputComponent(InputComponent);

}