// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/ChessPieces/BaseChessPiece.h"
#include "ChessPieceQueen.generated.h"

/**
 * 
 */
UCLASS()
class ZCHESS_API AChessPieceQueen : public ABaseChessPiece
{
    GENERATED_BODY()
	
	public:
        AChessPieceQueen();
	
        virtual void BeginPlay() override;
        virtual void Tick(float DeltaSeconds) override;
        virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};
