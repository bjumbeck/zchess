// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Actors/ChessPieces/BaseChessPiece.h"
#include "ChessPieceBishop.generated.h"

/// <summary>Represents the Bishop piece and all it's functionality.< / summary>
UCLASS()
class ZCHESS_API AChessPieceBishop : public ABaseChessPiece
{
    GENERATED_BODY()

    public:
        AChessPieceBishop();
	
        virtual void BeginPlay() override;
        virtual void Tick(float DeltaSeconds) override;
        virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};
