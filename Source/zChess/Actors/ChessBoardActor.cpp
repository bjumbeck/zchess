// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessBoardActor.h"
#include "Engine.h"

#include <string>
#include "Components/ChessTileComponent.h"

AChessBoardActor::AChessBoardActor()
{
    PrimaryActorTick.bCanEverTick = true;

    staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
    static ConstructorHelpers::FObjectFinderOptional<UStaticMesh> mesh(TEXT("StaticMesh'/Game/Meshes/Chessboard/chess_Board.chess_Board'"));
    staticMesh->SetStaticMesh(mesh.Get());
    staticMesh->SetRelativeScale3D(FVector(100.f, 100.f, 100.f));

    RootComponent = staticMesh;

    FVector tilePosition = FVector(-9.74f, -9.74f, 1.35f);
    float tilePlacementDistance = 2.78f;
    for (int y = 0; y != 8; ++y)
    {
        for (int x = 0; x != 8; ++x)
        {
            std::string stringPosition = ConvertNumberToLetter(y) + std::to_string(x + 1);
            UChessTileComponent* component = CreateDefaultSubobject<UChessTileComponent>(FName(stringPosition.c_str()));

            component->setTilePositionString(stringPosition.c_str());
            component->SetRelativeLocation(tilePosition);
            component->OnClicked.AddDynamic(this, &AChessBoardActor::OnBoardClicked);
            component->AttachTo(RootComponent);
            tiles.Add(component);

            tilePosition.X += tilePlacementDistance; // Move one tile over
        }

        tilePosition.Y += tilePlacementDistance; // Move one tile down
        tilePosition.X = -9.74f; // Reset to far left of the board
    }
}

void AChessBoardActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AChessBoardActor::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AChessBoardActor::OnBoardClicked(UPrimitiveComponent* component)
{
    if (GEngine)
    {
        UChessTileComponent* tile = Cast<UChessTileComponent>(component);
        GEngine->AddOnScreenDebugMessage(-1, 1.5f, FColor::Blue, TEXT("Tile Click: " + tile->getTilePositionString()));
    }
}

char AChessBoardActor::ConvertNumberToLetter(int number)
{
    char letter;
    switch (number)
    {
        case 0:
            letter = 'A';
            break;

        case 1:
            letter = 'B';
            break;

        case 2:
            letter = 'C';
            break;

        case 3:
            letter = 'D';
            break;

        case 4:
            letter = 'E';
            break;

        case 5:
            letter = 'F';
            break;

        case 6:
            letter = 'G';
            break;

        case 7:
            letter = 'H';
            break;

        default:
            letter = '*';
            UE_LOG(LogTemp, Warning, TEXT("AChessBoardActor::ConvertNumberToLetter(): Number to convert is out of range. Using default \"*\""));
            break;
    }

    return letter;
}