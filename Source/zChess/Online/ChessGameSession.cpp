// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessGameSession.h"
#include "Controllers/ChessPlayerController.h"

namespace
{
    const FString CustomMatchKeyword("Custom");
}

AChessGameSession::AChessGameSession(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
    if (!HasAnyFlags(RF_ClassDefaultObject))
    {
        // Link up callbacks to the delegates
        onCreateSessionCompleteDelegate = FOnCreateSessionCompleteDelegate::CreateUObject(this, &AChessGameSession::OnCreateSessionComplete);
        onDestroySessionCompleteDelegate = FOnDestroySessionCompleteDelegate::CreateUObject(this, &AChessGameSession::OnDestroySessionComplete);
        onFindSessionsCompleteDelegate = FOnFindSessionsCompleteDelegate::CreateUObject(this, &AChessGameSession::OnFindSessionsComplete);
        onJoinSessionCompleteDelegate = FOnJoinSessionCompleteDelegate::CreateUObject(this, &AChessGameSession::OnJoinSessionComplete);
        onStartSessionCompleteDelegate = FOnStartSessionCompleteDelegate::CreateUObject(this, &AChessGameSession::OnStartOnlineGameComplete);
    }
}

bool AChessGameSession::HostSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, const FString& gameType, const FString& mapName, bool isLAN, bool isPresence, int32 maxNumPlayers)
{
    const IOnlineSubsystem* onlineSubsystem = IOnlineSubsystem::Get();
    if (onlineSubsystem)
    {
        // Setup the session params
        currentSessionParams.SessionName = sessionName;
        currentSessionParams.IsLAN = isLAN;
        currentSessionParams.IsPresence = isPresence;
        currentSessionParams.UserId = userId;
        MaxPlayers = maxNumPlayers;

        // Setup the type of game we want to host and then host it.
        IOnlineSessionPtr sessions = onlineSubsystem->GetSessionInterface();
        if (sessions.IsValid() && currentSessionParams.UserId.IsValid())
        {
            hostSettings = MakeShareable(new FChessOnlineSessionSettings(isLAN, isPresence, maxNumPlayers));
            hostSettings->Set(SETTING_GAMEMODE, gameType, EOnlineDataAdvertisementType::ViaOnlineService);
            hostSettings->Set(SETTING_MAPNAME, mapName, EOnlineDataAdvertisementType::ViaOnlineService);
            hostSettings->Set(SETTING_MATCHING_HOPPER, FString("ChessGame"), EOnlineDataAdvertisementType::DontAdvertise);
            hostSettings->Set(SETTING_MATCHING_TIMEOUT, 120.0f, EOnlineDataAdvertisementType::ViaOnlineService);
            hostSettings->Set(SETTING_SESSION_TEMPLATE_NAME, FString("GameSession"), EOnlineDataAdvertisementType::DontAdvertise);
            hostSettings->Set(SEARCH_KEYWORDS, CustomMatchKeyword, EOnlineDataAdvertisementType::ViaOnlineService);

            OnCreateSessionCompleteDelegateHandle = sessions->AddOnCreateSessionCompleteDelegate_Handle(onCreateSessionCompleteDelegate);

            return sessions->CreateSession(*currentSessionParams.UserId, currentSessionParams.SessionName, *hostSettings);
        }
    }

    return false;
}

void AChessGameSession::FindSessions(TSharedPtr<FUniqueNetId> userId, FName sessionName, bool isLAN, bool isPresence)
{
    IOnlineSubsystem* onlineSubsystem = IOnlineSubsystem::Get();
    if (onlineSubsystem)
    {
        currentSessionParams.SessionName = sessionName;
        currentSessionParams.IsLAN = isLAN;
        currentSessionParams.IsPresence = isPresence;
        currentSessionParams.UserId = userId;

        IOnlineSessionPtr sessions = onlineSubsystem->GetSessionInterface();
        if (sessions.IsValid() && currentSessionParams.UserId.IsValid())
        {
            searchSettings = MakeShareable(new FChessOnlineSearchSettings(isLAN, isPresence));
            searchSettings->QuerySettings.Set(SEARCH_KEYWORDS, CustomMatchKeyword, EOnlineComparisonOp::Equals);

            TSharedRef<FOnlineSessionSearch> searchSettingsRef = searchSettings.ToSharedRef();

            OnFindSessionsCompleteDelegateHandle = sessions->AddOnFindSessionsCompleteDelegate_Handle(onFindSessionsCompleteDelegate);
            sessions->FindSessions(*currentSessionParams.UserId, searchSettingsRef);
        }
    }
    else
    {
        OnFindSessionsComplete(false);
    }
}

bool AChessGameSession::JoinSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, int32 sessionIndexInSearchResults)
{
    bool result = false;

    // Test if we are trying to join a valid session index within the session list
    if (sessionIndexInSearchResults >= 0 && sessionIndexInSearchResults < searchSettings->SearchResults.Num())
    {
        result = JoinSession(userId, sessionName, searchSettings->SearchResults[sessionIndexInSearchResults]);
    }

    return result;
}

bool AChessGameSession::JoinSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, const FOnlineSessionSearchResult& searchResult)
{
    bool result = false;

    IOnlineSubsystem* onlineSubsystem = IOnlineSubsystem::Get();
    if (onlineSubsystem)
    {
        IOnlineSessionPtr sessions = onlineSubsystem->GetSessionInterface();
        if (sessions.IsValid() && userId.IsValid())
        {
            OnJoinSessionCompleteDelegateHandle = sessions->AddOnJoinSessionCompleteDelegate_Handle(onJoinSessionCompleteDelegate);
            result = sessions->JoinSession(*userId, sessionName, searchResult);
        }
    }

    return result;
}

bool AChessGameSession::IsAsyncBusy() const
{
    if (hostSettings.IsValid() || searchSettings.IsValid())
    {
        return true;
    }

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            EOnlineSessionState::Type gameSessionSate = sessions->GetSessionState(GameSessionName); // TODO: Is this the right argument to pass?
            EOnlineSessionState::Type partySessionState = sessions->GetSessionState(PartySessionName); // TODO: Is this the right argument to pass?

            if (gameSessionSate != EOnlineSessionState::NoSession || partySessionState != EOnlineSessionState::NoSession)
            {
                return true;
            }
        }
    }

    return false;
}

EOnlineAsyncTaskState::Type AChessGameSession::GetSearchResultStatus(int32& searchResultIndex, int32& numSearchResults)
{
    searchResultIndex = 0;
    numSearchResults = 0;

    if (searchSettings.IsValid())
    {
        if (searchSettings->SearchState == EOnlineAsyncTaskState::Done)
        {
            searchResultIndex = currentSessionParams.BestSessionIndex;
            numSearchResults = searchSettings->SearchResults.Num();
        }

        return searchSettings->SearchState;
    }

    return EOnlineAsyncTaskState::NotStarted;
}

const TArray<FOnlineSessionSearchResult>& AChessGameSession::GetSearchResults() const
{
    return searchSettings->SearchResults;
}

void AChessGameSession::HandleMatchHasStarted()
{
    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            UE_LOG(LogOnlineGame, Log, TEXT("Starting session %s on server"), *GameSessionName.ToString());
            OnStartSessionCompleteDelegateHandle = sessions->AddOnStartSessionCompleteDelegate_Handle(onStartSessionCompleteDelegate);
            sessions->StartSession(GameSessionName);
        }
    }
}

void AChessGameSession::HandleMatchHasEnded()
{
    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            // Tell all clients to end
            for (FConstPlayerControllerIterator iter = GetWorld()->GetPlayerControllerIterator(); iter; ++iter)
            {
                AChessPlayerController* pc = Cast<AChessPlayerController>(*iter);
                if (pc && !pc->IsLocalPlayerController())
                {
                    pc->ClientEndOnlineGame();
                }
            }

            // Handle the server here
            UE_LOG(LogOnlineGame, Log, TEXT("Ending session %s on server"), *GameSessionName.ToString());
            sessions->EndSession(GameSessionName);
        }
    }
}

bool AChessGameSession::TravelToSession(int32 controllerId, FName sessionName)
{
    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        FString url;
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid() && sessions->GetResolvedConnectString(sessionName, url))
        {
            APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), controllerId);
            if (pc)
            {
                pc->ClientTravel(url, TRAVEL_Absolute);
                return true;
            }
        }
        else
        {
            UE_LOG(LogOnlineGame, Warning, TEXT("Failed to join session %s"), *sessionName.ToString());
        }
    }
    #if !UE_BUILD_SHIPPING
    else
    {
        APlayerController* pc = UGameplayStatics::GetPlayerController(GetWorld(), controllerId);
        if (pc)
        {
            FString localURL(TEXT("127.0.0.1"));
            pc->ClientTravel(localURL, TRAVEL_Absolute);
            return true;
        }
    }
    #endif

    return false;
}

void AChessGameSession::OnCreateSessionComplete(FName sessionName, bool wasSuccessful)
{
    UE_LOG(LogOnlineGame, Verbose, TEXT("OnCreateSessionComplete %s wasSuccessful: %d"), *sessionName.ToString(), wasSuccessful);

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        sessions->ClearOnCreateSessionCompleteDelegate_Handle(OnCreateSessionCompleteDelegateHandle);
    }

    OnCreatePresenceSessionComplete().Broadcast(sessionName, wasSuccessful);
}

void AChessGameSession::OnStartOnlineGameComplete(FName sessionName, bool wasSuccessful)
{
    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            sessions->ClearOnStartSessionCompleteDelegate_Handle(OnStartSessionCompleteDelegateHandle);
        }
    }

    if (wasSuccessful)
    {
        // Tell non-local players to start an online game.
        for (FConstPlayerControllerIterator iter = GetWorld()->GetPlayerControllerIterator(); iter; ++iter)
        {
            AChessPlayerController* pc = Cast<AChessPlayerController>(*iter);
            if (pc && !pc->IsLocalPlayerController())
            {
                pc->ClientStartOnlineGame();
            }
        }
    }
}

void AChessGameSession::OnFindSessionsComplete(bool wasSuccessful)
{
    UE_LOG(LogOnlineGame, Verbose, TEXT("OnFindSessionsComplete wasSuccessful: %d"), wasSuccessful);

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        if (sessions.IsValid())
        {
            sessions->ClearOnFindSessionsCompleteDelegate_Handle(OnFindSessionsCompleteDelegateHandle);

            UE_LOG(LogOnlineGame, Verbose, TEXT("Number Search Results: %d"), searchSettings->SearchResults.Num());
            for (int32 searchIndex = 0; searchIndex < searchSettings->SearchResults.Num(); ++searchIndex)
            {
                const FOnlineSessionSearchResult& searchResult = searchSettings->SearchResults[searchIndex];
                DumpSession(&searchResult.Session);
            }

            OnFindSessionsComplete().Broadcast(wasSuccessful);
        }
    }
}

void AChessGameSession::OnJoinSessionComplete(FName sessionName, EOnJoinSessionCompleteResult::Type result)
{
    bool willTravel = false;

    UE_LOG(LogOnlineGame, Verbose, TEXT("OnJoinSessionComplete %s Success: %d"), *sessionName.ToString(), static_cast<int32>(result));

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    IOnlineSessionPtr sessions = NULL;
    if (onlineSub)
    {
        sessions = onlineSub->GetSessionInterface();
        sessions->ClearOnJoinSessionCompleteDelegate_Handle(OnJoinSessionCompleteDelegateHandle);
    }

    OnJoinSessionComplete().Broadcast(result);
}

void AChessGameSession::OnDestroySessionComplete(FName sessionName, bool wasSuccessful)
{
    UE_LOG(LogOnlineGame, Verbose, TEXT("OnDestroySessionComplete %s Success %d"), *sessionName.ToString(), wasSuccessful);

    IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
    if (onlineSub)
    {
        IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
        sessions->ClearOnDestroySessionCompleteDelegate_Handle(OnDestroySessionCompleteDelegateHandle);
        hostSettings = NULL;
    }
}

void AChessGameSession::ResetBestSessionVars()
{
    currentSessionParams.BestSessionIndex = -1;
}

void AChessGameSession::ChooseBestSession()
{
    // Start the search from where we left off
    for (int32 sessionIndex = currentSessionParams.BestSessionIndex + 1; sessionIndex < searchSettings->SearchResults.Num(); ++sessionIndex)
    {
        currentSessionParams.BestSessionIndex = sessionIndex;
        return;
    }

    currentSessionParams.BestSessionIndex = -1;
}

void AChessGameSession::StartMatchmaking()
{
    ResetBestSessionVars();
    ContinueMatchmaking();
}

void AChessGameSession::ContinueMatchmaking()
{
    ChooseBestSession();
    if (currentSessionParams.BestSessionIndex >= 0 && currentSessionParams.BestSessionIndex < searchSettings->SearchResults.Num())
    {
        IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
        if (onlineSub)
        {
            IOnlineSessionPtr session = onlineSub->GetSessionInterface();
            if (session.IsValid() && currentSessionParams.UserId.IsValid())
            {
                OnJoinSessionCompleteDelegateHandle = session->AddOnJoinSessionCompleteDelegate_Handle(onJoinSessionCompleteDelegate);
                session->JoinSession(*currentSessionParams.UserId, currentSessionParams.SessionName, searchSettings->SearchResults[currentSessionParams.BestSessionIndex]);
            }
        }
    }
    else
    {
        OnNoMatchesAvailable();
    }
}

void AChessGameSession::OnNoMatchesAvailable()
{
    UE_LOG(LogOnlineGame, Verbose, TEXT("Matchmaking complete, no sessions available."));
    searchSettings = NULL;
}