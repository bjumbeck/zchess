// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Online.h"
#include "GameFramework/GameSession.h"
#include "ChessGameSession.generated.h"

struct FChessGameSessionParams
{
    FChessGameSessionParams()
        : SessionName(NAME_None)
        , IsLAN(false)
        , IsPresence(false)
        , BestSessionIndex(0)
    {
    }

    /// Name of the session.
    FName SessionName;
    /// Is LAN Match.
    bool IsLAN;
    /// Presence enabled session.
    bool IsPresence;
    /// Identifier for the user initiating the lobby.
    TSharedPtr<FUniqueNetId> UserId;
    /// Current search result to join
    int32 BestSessionIndex;
};

/**
 * 
 */
UCLASS()
class ZCHESS_API AChessGameSession : public AGameSession
{
    GENERATED_BODY()

    protected:
        // These need to be before some of the other methods to work so I am placing them at the top of the class.

        #pragma region Documentation
        /// Event triggered when a session is created
        ///
        /// @param SessionName      Name of the session that was created
        /// @param WasSuccessful    Was the creation successful
        #pragma endregion
        DECLARE_EVENT_TwoParams(AChessGameSession, FOnCreatePresenceSessionComplete, FName /*SessionName*/, bool /*WasSuccessful*/);
        FOnCreatePresenceSessionComplete createPresenceSessionCompleteEvent;

        #pragma region Documentation
        /// Event triggered when a session is joined
        ///
        /// @param Result           Enum that can be used to see the result of the join (Failure, Success, etc.)
        #pragma endregion
        DECLARE_EVENT_OneParam(AChessGameSession, FOnJoinSessionComplete, EOnJoinSessionCompleteResult::Type /*Result*/);
        FOnJoinSessionComplete joinSessionCompleteEvent;

        #pragma region Documentation
        /// Event triggered after a session search completes
        ///
        /// @param WasSuccessful    Was the search successful
        #pragma endregion
        DECLARE_EVENT_OneParam(AChessGameSession, FOnFindSessionsComplete, bool /*WasSuccessful*/);
        FOnFindSessionsComplete findSessionsCompleteEvent;
	
    public:
        AChessGameSession(const FObjectInitializer& ObjectInitializer);

        #pragma region Documentation
        /// Host a new online session
        ///
        /// @param userId           User the initiated the request
        /// @param sessionName      Name of the session
        /// @param gameType         The type of game to use
        /// @param mapName          The name of the map to use
        /// @param isLAN            Is this going to be hosted over LAN
        /// @param isPresence       Is the session to create a presence session
        /// @param maxNumPlayers    Maximum number of players to allow in the session
        ///
        /// @return bool true if successful, false otherwise
        #pragma endregion
        bool HostSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, const FString& gameType, const FString& mapName, bool isLAN, bool isPresence, int32 maxNumPlayers);

        #pragma region Documentation
        /// Find an online session
        ///
        /// @param userId           User that initiated the request
        /// @param sessionName      Name of the session
        /// @param isLAN            Are we searching LAN matches
        /// @param isPresence       Are we searching presence sessions
        #pragma endregion
        void FindSessions(TSharedPtr<FUniqueNetId> userId, FName sessionName, bool isLAN, bool isPresence);

        #pragma region Documentation
        /// Joins one of the sessions in search results
        ///
        /// @param userId                       User that initiated the request
        /// @param sessionName                  Name of the session
        /// @param sessionIndexInSearchResults  The session index in search results.
        ///
        /// @return bool true if it succeeds, false if it fails
        #pragma endregion
        bool JoinSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, int32 sessionIndexInSearchResults);

        #pragma region Documentation
        /// Join a session via a search result
        ///
        /// @param userId           User that initiated the request
        /// @param sessionName      Name of the session
        /// @param searchResult     The session to join
        ///
        /// @return true if it succeeds, false if it fails
        #pragma endregion
        bool JoinSession(TSharedPtr<FUniqueNetId> userId, FName sessionName, const class FOnlineSessionSearchResult& searchResult);

        #pragma region Documentation
        /// Determine whether any online async work is in progress.
        ///
        /// @return true if online async work is in progress, false if not
        #pragma endregion
        bool IsAsyncBusy() const;

        #pragma region Documentation
        /// Get the search results found and the current search result being probed
        ///
        /// @param searchResultIndex    Index of the current search result accessed
        /// @param numSearchResults     Number of total search results found in FindGame()
        ///
        /// @return State of the search result query
        #pragma endregion
        EOnlineAsyncTaskState::Type GetSearchResultStatus(int32& searchResultIndex, int32& numSearchResults);

        #pragma region Documentation
        /// Gets the search results.
        ///
        /// @return The search results
        #pragma endregion
        const TArray<FOnlineSessionSearchResult>& GetSearchResults() const;

        /// @return The delegate fired when creating a presence session
        FOnCreatePresenceSessionComplete& OnCreatePresenceSessionComplete() { return createPresenceSessionCompleteEvent; }
        /// @return The delegate fired when joining a session
        FOnJoinSessionComplete& OnJoinSessionComplete() { return joinSessionCompleteEvent; }
        /// @return The delegate fired when search of sessions completes
        FOnFindSessionsComplete& OnFindSessionsComplete() { return findSessionsCompleteEvent; }

        /// Handles starting the match
        virtual void HandleMatchHasStarted() override;
        /// Handles when the match has ended
        virtual void HandleMatchHasEnded() override;

        #pragma region Documentation
        /// Travel to a session URL (client) for a given session
        ///
        /// @param controllerId     Controller initiating the session travel
        /// @param sessionName      Name of the session to travel to
        ///
        /// @return true if it succeeds, false if it fails
        #pragma endregion
        bool TravelToSession(int32 controllerId, FName sessionName);

    protected:
        #pragma region Documentation
        /// Delegate fired when a session create request has completed
        ///
        /// @param sessionName      The name of the session this callback is for
        /// @param wasSuccessful    True if the async action completed without error, false if there was an error
        #pragma endregion
        virtual void OnCreateSessionComplete(FName sessionName, bool wasSuccessful);

        #pragma region Documentation
        /// Delegate fired when a session start request has completed
        ///
        /// @param sessionName      The name of the session this callback is for
        /// @param wasSuccessful    True if the async action completed without error, false if there was an error
        #pragma endregion
        void OnStartOnlineGameComplete(FName sessionName, bool wasSuccessful);

        #pragma region Documentation
        /// Delegate fired when a session search query has completed
        ///
        /// @param wasSuccessful    True if the async action completed without error, false if there was an error
        #pragma endregion
        void OnFindSessionsComplete(bool wasSuccessful);

        #pragma region Documentation
        /// Delegate fired when a session join request has completed
        ///
        /// @param sessionName      The name of the session this callback is for
        /// @param result           The result of the join request (Failure, Success, etc.)
        #pragma endregion
        void OnJoinSessionComplete(FName sessionName, EOnJoinSessionCompleteResult::Type result);

        #pragma region Documentation
        /// Delegate fired when a destroying an online session has completed
        ///
        /// @param sessionName      The name of the session this callback is for
        /// @param wasSuccessful    True if the async action completed without error, false if there was an error
        #pragma endregion
        virtual void OnDestroySessionComplete(FName sessionName, bool wasSuccessful);

        /// Reset the variables the are keeping track of session join attempts
        void ResetBestSessionVars();
        /// Choose the best session from a list of search results based on game criteria
        void ChooseBestSession();

        /// Entry point for matchmaking after search results are returned
        void StartMatchmaking();
        /// Return point after each failed attempt to join a search result
        void ContinueMatchmaking();

        /// Delegate triggered when no more search results are available
        void OnNoMatchesAvailable();
	
	public:
        /// The default number of players allowed in a game
        static const int32 DEFAULT_NUM_PLAYERS = 2;	

        /// Handles for various registered delegates
        FDelegateHandle OnStartSessionCompleteDelegateHandle;
        FDelegateHandle OnCreateSessionCompleteDelegateHandle;
        FDelegateHandle OnDestroySessionCompleteDelegateHandle;
        FDelegateHandle OnFindSessionsCompleteDelegateHandle;
        FDelegateHandle OnJoinSessionCompleteDelegateHandle;

    protected:
        /// Delegate for creating a new session
        FOnCreateSessionCompleteDelegate onCreateSessionCompleteDelegate;
        /// Delegate that launches after starting a session
        FOnStartSessionCompleteDelegate onStartSessionCompleteDelegate;
        /// Delegate for destroying a session. 
        FOnDestroySessionCompleteDelegate onDestroySessionCompleteDelegate;
        /// Delegate for searching for sessions
        FOnFindSessionsCompleteDelegate onFindSessionsCompleteDelegate;
        /// Delegate that launches after joining a session
        FOnJoinSessionCompleteDelegate onJoinSessionCompleteDelegate;

        /// Transient properties of a session during game creation and matchmaking
        FChessGameSessionParams currentSessionParams;

        /// Current host settings
        TSharedPtr<class FChessOnlineSessionSettings> hostSettings;
        /// Current search settings
        TSharedPtr<class FChessOnlineSearchSettings> searchSettings;
};

class FChessOnlineSessionSettings : public FOnlineSessionSettings
{
    public:
        FChessOnlineSessionSettings(bool isLAN = false, bool isPresence = false, int32 maxNumPlayers = 2)
        {
            NumPublicConnections = maxNumPlayers;
            if (NumPublicConnections < 0)
                NumPublicConnections = 0;

            NumPrivateConnections = 0;
            bIsLANMatch = isLAN;
            bShouldAdvertise = true;
            bAllowJoinInProgress = false;
            bAllowInvites = true;
            bUsesPresence = isPresence;
            bAllowJoinViaPresence = true;
            bAllowJoinViaPresenceFriendsOnly = false;
        }
};

class FChessOnlineSearchSettings : public FOnlineSessionSearch
{
    public:
        FChessOnlineSearchSettings(bool searchingLAN = false, bool searchingPresence = false)
        {
            bIsLanQuery = searchingLAN;
            MaxSearchResults = 10;
            PingBucketSize = 50;

            if (searchingPresence)
                QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
        }
};