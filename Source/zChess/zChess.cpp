// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "UI/Styles/ChessStyle.h"

// Custom Game Module
class FChessGameModule : public FDefaultGameModuleImpl
{
    virtual void StartupModule() override
    {
        FSlateStyleRegistry::UnRegisterSlateStyle(FChessStyle::GetStyleSetName());
        FChessStyle::Initialize();
    }

    virtual void ShutdownModule() override
    {
        FChessStyle::Shutdown();
    }
};

IMPLEMENT_PRIMARY_GAME_MODULE(FChessGameModule, zChess, "zChess");

DEFINE_LOG_CATEGORY(LogChess)