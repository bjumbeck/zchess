// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPlayerController.h"
#include "Player/ChessLocalPlayer.h"
#include "Player/ChessPersistentUser.h"
#include "Player/ChessPlayerState.h"
#include "ChessGameInstance.h"

AChessPlayerController::AChessPlayerController(const FObjectInitializer& objectInitializer)
    : Super(objectInitializer)
{
    // PlayerCameraManager = AChessPlayerCameraManager::StaticClass();    TODO: Create camera manager class
    // CheatClass = UChessCheatManager::StaticClass();   TODO: Create cheat class
    
    bShowMouseCursor = true;
    bEnableClickEvents = true;
    DefaultMouseCursor = EMouseCursor::Default;

    bAllowGameActions = true;
    bGameEndedFrame = false;

    serverSayString = TEXT("Say");
    chessFriendUpdateTimer = 0.0f;
    hasSentStartEvents = false;
}

// AChessPlayerController::ClientSetSpectatorCamera's implementation
void AChessPlayerController::ClientSetSpectatorCamera_Implementation(FVector cameraLocation, FRotator cameraRotation)
{
    SetInitialLocationAndRotation(cameraLocation, cameraRotation);
    SetViewTarget(this);
}

// AChessPlayerController::ClientGameStarted's implementation
void AChessPlayerController::ClientGameStarted_Implementation()
{
    bAllowGameActions = true;

    // Enables controls now that the game has started.
    SetIgnoreMoveInput(false);

    // TODO: Implement after the chess HUD is created.
//     AChessHUD* chessHUD = GetChessHUD();
//     if (chessHUD)
//     {
//         chessHUD->SetMatchState(EChessMatchState::Playing);
//         chessHUD->ShowScoreboard(false);
//     }

    bGameEndedFrame = false;

    // Send the round start event
    const auto events = Online::GetEventsInterface();
    ULocalPlayer* localPlayer = Cast<ULocalPlayer>(Player);

    if (localPlayer != nullptr && events.IsValid())
    {
        auto uniqueId = localPlayer->GetPreferredUniqueNetId();

        if (uniqueId.IsValid())
        {
            // Generate a new session Id
            events->SetPlayerSessionId(*uniqueId, FGuid::NewGuid());
            
            FString mapName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName()); // TODO: Find out what this means.

            // Fire session start event
            FOnlineEventParms params;
            params.Add(TEXT("GameplayModeId"), FVariantData(static_cast<int32>(1))); // TODO: Find out what this means.
            params.Add(TEXT("DifficultyLevelId"), FVariantData(static_cast<int32>(0))); // TODO: Find out what this means.
            params.Add(TEXT("MapName"), FVariantData(mapName));

            events->TriggerEvent(*uniqueId, TEXT("PlayerSessionStart"), params); // Trigger PlayerSessionStart event

            UChessGameInstance* chessGameInstance = GetWorld() != NULL ? Cast<UChessGameInstance>(GetWorld()->GetGameInstance()) : NULL;
            FOnlineEventParms multiplayerParams;

            multiplayerParams.Add(TEXT("SectionId"), FVariantData(static_cast<int32>(0))); // Not used.
            multiplayerParams.Add(TEXT("GameplayModeId"), FVariantData(static_cast<int32>(1)));
            multiplayerParams.Add(TEXT("MatchTypeId"), FVariantData(static_cast<int32>(1)));
            multiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData(static_cast<int32>(0))); // Not used.

            events->TriggerEvent(*uniqueId, TEXT("MultiplayerRoundStart"), multiplayerParams);

            hasSentStartEvents = true;
        }
    }
}

void AChessPlayerController::ClientStartOnlineGame_Implementation()
{
    if (!IsPrimaryPlayer())
        return;

    AChessPlayerState* chessPlayerState = Cast<AChessPlayerState>(PlayerState);
    if (chessPlayerState)
    {
        IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
        if (onlineSub)
        {
            IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
            if (sessions.IsValid())
            {
                UE_LOG(LogOnline, Log, TEXT("Starting session %s on client"), *chessPlayerState->SessionName.ToString());
                sessions->StartSession(chessPlayerState->SessionName);
            }
        }
    }
    else
    {
        // Keep retrying until the player state is replicated
        GetWorld()->GetTimerManager().SetTimer(timerHandle_ClientStartOnlineGame, this, &AChessPlayerController::ClientStartOnlineGame_Implementation, 0.2f, false);
    }
}

void AChessPlayerController::ClientEndOnlineGame_Implementation()
{
    if (!IsPrimaryPlayer())
        return;

    AChessPlayerState* chessPlayerState = Cast<AChessPlayerState>(PlayerState);
    if (chessPlayerState)
    {
        IOnlineSubsystem* onlineSub = IOnlineSubsystem::Get();
        if (onlineSub)  
        {
            IOnlineSessionPtr sessions = onlineSub->GetSessionInterface();
            if (sessions.IsValid())
            {
                UE_LOG(LogOnline, Log, TEXT("Ending session %s on client"), *chessPlayerState->SessionName.ToString());
                sessions->EndSession(chessPlayerState->SessionName);
            }
        }
    }
}

void AChessPlayerController::ClientSendRoundEndEvent_Implementation(bool isWinner, int32 expendedTimeInSeconds)
{   
    const auto events = Online::GetEventsInterface();
    ULocalPlayer* localPlayer = Cast<ULocalPlayer>(Player);

    // TODO: Finish after implement AChessPlayerState class
//     if (hasSentStartEvents && localPlayer != nullptr && events.IsValid())
//     {
//         auto uniqueId = localPlayer->GetPreferredUniqueNetId();
// 
//         if (uniqueId.IsValid())
//         {
//             FString mapName = *FPackageName::GetShortName(GetWorld()->PersistentLevel->GetOutermost()->GetName());
//             AChessPlayerState* chessPlayerState = Cast<AChessPlayerState>(PlayerState);
// 
//             // Fire session end event for all cases
//             FOnlineEventParms params;
//             params.Add(TEXT("GameplayModeId"), FVariantData(static_cast<int32>(1))); // TODO: Determine gamemode
//             params.Add(TEXT("DifficultyLevelId"), FVariantData(static_cast<int32>(0))); // not used
//             params.Add(TEXT("ExitStatusId"), FVariantData(static_cast<int32>(0))); // not used
//             params.Add(TEXT("PlayerWon"), FVariantData(static_cast<bool>(isWinner)));
//             params.Add(TEXT("MapName"), FVariantData(mapName));
//             params.Add(TEXT("MapNameString"), FVariantData(mapName));
// 
//             events->TriggerEvent(*uniqueId, TEXT("PlayerSessionEnd"), params);
// 
//             // Online Version
//             UChessGameInstance* chessGameInstance = GetWorld() != NULL ? Cast<UChessGameInstance>(GetWorld()->GetGameInstance()) : NULL;
//             if (chessGameInstance->GetIsOnline())
//             {
//                 FOnlineEventParms multiplayerParams;
// 
//                 const AChessGameState* myGameState = GetWorld() != NULL ? GetWorld()->GetGameState<AChessGameState>() : NULL;
//                 if (ensure(myGameState != nullptr))
//                 {
//                     multiplayerParams.Add(TEXT("SectionId"), FVariantData(static_cast<int32>(0))); // Not used
//                     multiplayerParams.Add(TEXT("GameplayModeId"), FVariantData(static_cast<int32>(1))); // TODO: Determine game mode
//                     multiplayerParams.Add(TEXT("MatchTypeId"), FVariantData(static_cast<int32>(1)));
//                     multiplayerParams.Add(TEXT("DifficultyLevelId"), FVariantData(static_cast<int32>(0))); // Not used
//                     multiplayerParams.Add(TEXT("TimeInSeconds"), FVariantData(static_cast<float>(expendedTimeInSeconds)));
//                     multiplayerParams.Add(TEXT("ExitStatusId"), FVariantData(static_cast<int32>(0))); // Not used
// 
//                     events->TriggerEvent(*uniqueId, TEXT("MultiplayerRoundEnd"), multiplayerParams);
//                 }
//             }
//         }
// 
//         hasSentStartEvents = false;
//     }
}

void AChessPlayerController::ClientGameEnded_Implementation(AActor* endGameFocus, bool isWinner)
{
    Super::ClientGameEnded_Implementation(endGameFocus, isWinner);

    // Disable controls now that the game has ended
    SetIgnoreMoveInput(true);
    bAllowGameActions = false;

    // Make sure we still have a valid view target
    SetViewTarget(GetPawn()); // TODO: Is this correct for this game

    // TODO: Implement after the AChessHUD header is created.
//     AChessHUD* chessHUD = GetChessHUD();
//     if (chessHUD)
//     {
//         chessHUD->SetMatchState(isWinner ? EChessMatchState::Won : EChessMatchState::Lost);
//     }

    UpdateSaveFileOnGameEnd(isWinner);
    // UpdateAchievementsOnGameEnd();
    // UpdateLeaderboardsOnGameEnd();
    
    // Flag that the game has just ended
    bGameEndedFrame = true;
}

void AChessPlayerController::ClientTeamMessage_Implementation(APlayerState* senderPlayerState, const FString& str, FName type, float msgLifeTime)
{
        // TODO: Implement after AChessHUD header is created.
//     AChessHUD* chessHUD = Cast<AChessHUD>(GetHUD());
//     if (chessHUD)
//     {
//         if (type == serverSayString)
//         {
//             if (senderPlayerState != PlayerState)
//             {
//                 chessHUD->AddChatLine(str, false);
//             }
//         }
//     }
}

void AChessPlayerController::ToggleChatWindow()
{
        // TODO: Implement after AChessHUD header is created.
//     AChessHUD* chessHUD = Cast<AChessHUD>(GetHUD());
//     if (chessHUD)
//     {
//         chessHUD->ToggleChat();
//     }
}

void AChessPlayerController::Say(const FString& msg)
{
    ServerSay(msg.Left(128));
}

bool AChessPlayerController::ServerSay_Validate(const FString& msg)
{
    return true;
}

void AChessPlayerController::ServerSay_Implementation(const FString& msg)
{
    GetWorld()->GetAuthGameMode()->Broadcast(this, msg, serverSayString);
}

void AChessPlayerController::OnToggleInGameMenu()
{
    // Not ideal, but it is necessary to prevent both players from pausing at the same time on the same frame.
    UWorld* gameWorld = GEngine->GameViewport->GetWorld();

    for (auto iter = gameWorld->GetControllerIterator(); iter; ++iter)
    {
        AChessPlayerController* controller = Cast<AChessPlayerController>(*iter);
        if (controller && controller->IsPaused())
        {
            return;
        }
    }

    // If no one has paused we can pause the game
    // TODO: Implement after we have created the ChessInGameMenu header
//     if (chessInGameMenu.IsValid())
//     {
//         chessInGameMenu->ToggleGameMenu();
//     }   
}

// TODO: Implement after creating the AChessHUD class
// AChessHUD* AChessPlayerController::GetChessHUD() const
// {
//     return Cast<AChessHUD>(GetHUD());
// }

void AChessPlayerController::ShowInGameMenu()
{
    // TODO: Implement after we have created the ChessInGameMenu header and ChessHUD header
//     AChessHUD* chessHUD = GetChessHUD();
//     if (chessInGameMenu.IsValid() && !chessInGameMenu->GetIsGameMenuUp() && chessHUD && (chessHUD->IsMatchOver() == false))
//     {
//         chessInGameMenu->ToggleGameMenu();
//     }
}

bool AChessPlayerController::IsGameInputAllowed() const
{
    return bAllowGameActions && !bCinematicMode;
}

bool AChessPlayerController::IsGameMenuVisible() const
{
    bool result = false;

    // TODO: Implement after we have created the ChessInGameMenu header
//     if (chessInGameMenu.IsValid())
//     {
//         result = chessInGameMenu->GetIsGameMenuUp();
//     }

    return result;
}

void AChessPlayerController::CleanupSessionOnReturnToMenu()
{
    UChessGameInstance* gameInstance = GetWorld() != NULL ? Cast<UChessGameInstance>(GetWorld()->GetGameInstance()) : NULL;
    if (ensure(gameInstance != NULL))
    {
        gameInstance->CleanupSessionOnReturnToMenu();
    }
}

void AChessPlayerController::InitInputSystem()
{
    Super::InitInputSystem();

    // If you need to do anything with the base input settings do it here.
}

bool AChessPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate /* = FCanUnpause() */)
{
    // Call to the super
    const bool result = APlayerController::SetPause(bPause, CanUnpauseDelegate);

    // Update rich presence
    const auto presenceInterface = Online::GetPresenceInterface();
    const auto events = Online::GetEventsInterface();
    const auto localPlayer = Cast<ULocalPlayer>(Player);
    TSharedPtr<FUniqueNetId> userId = localPlayer ? localPlayer->GetCachedUniqueNetId() : nullptr;

    if (presenceInterface.IsValid() && userId.IsValid())
    {
        FOnlineUserPresenceStatus presenceStatus;
        if (result && bPause)
        {
            presenceStatus.Properties.Add(DefaultPresenceKey, FString("Paused"));
        }
        else
        {
            presenceStatus.Properties.Add(DefaultPresenceKey, FString("InGame"));
        }

        presenceInterface->SetPresence(*userId, presenceStatus);
    }

    // Don't send pause events while online since the game can't pause in online play.
    if (GetNetMode() == NM_Standalone && events.IsValid() && PlayerState->UniqueId.IsValid())
    {
        FOnlineEventParms params;
        params.Add(TEXT("GameplayModeId"), FVariantData(static_cast<int32>(1)));
        params.Add(TEXT("DifficultyLevelId"), FVariantData(static_cast<int32>(0))); // Not used.

        if (result && bPause)
        {
            events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionPause"), params);
        }
        else
        {
            events->TriggerEvent(*PlayerState->UniqueId, TEXT("PlayerSessionResume"), params);
        }
    }

    return result;
}

UChessPersistentUser* AChessPlayerController::GetPersistentUser() const
{
    const UChessLocalPlayer* chessLocalPlayer = Cast<UChessLocalPlayer>(Player);
    return chessLocalPlayer ? chessLocalPlayer->GetPersistentUser() : nullptr;
}

void AChessPlayerController::HandleReturnToMainMenu()
{
    CleanupSessionOnReturnToMenu();
}

void AChessPlayerController::SetPlayer(UPlayer* player)
{
    Super::SetPlayer(player);

    // Build the in-game menu only after the game is initialized
    // TODO: Implement after we have created the FChessInGameMenu header
//     chessInGameMenu = MakeShareable(new FChessInGameMenu());
//     chessInGameMenu->Construct(Cast<ULocalPlayer>(Player));
}

void AChessPlayerController::PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
    Super::PreClientTravel(PendingURL, TravelType, bIsSeamlessTravel);

    if (GetWorld() != NULL)
    {
        // TODO: Implement after we have created the UChessGameViewportClient header.
//         UChessGameViewportClient* chessViewport = Cast<UChessGameViewportClient>(GetWorld()->GetGameViewport());
// 
//         if (chessViewport != NULL)
//         {   
//             chessViewport->ShowLoadingScreen();
//         }

        // TODO: Implement after we have created the AChessHUD header. Also do we even need this?
//         AChessHUD* chessHUD = Cast<AChessHUD>(GetHUD());
//         if (chessHUD != nullptr)
//         {  
//             // Need to pass true to bFocus here so that the focus is returned to the game view port.
//             chessHUD->ShowScoreboard(false, true);
//         }
    }
}

void AChessPlayerController::PostInitializeComponents()
{
    Super::PostInitializeComponents();

    // FChessStyle::Initialize // TODO: Implement after we create the FChessStyle header.
    chessFriendUpdateTimer = 0;
}

void AChessPlayerController::BeginPlay()
{
    Super::BeginPlay();

    // Do whatever is need on the start of play here.
}

void AChessPlayerController::TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction)
{
    Super::TickActor(DeltaTime, TickType, ThisTickFunction);

    if (IsGameMenuVisible())
    {
        if (chessFriendUpdateTimer > 0)
        {
            chessFriendUpdateTimer -= DeltaTime;
        }
        else
        {
                // TODO: Implement after we have created the FChessFriends and ChessInGameMenu class headers.
//             TSharedPtr<class FChessFriends> chessFriends = chessInGameMenu->GetChessFriends();
//             ULocalPlayer* localPlayer = Cast<ULocalPlayer>(Player);
//             if (chessFriends.IsValid() && localPlayer && localPlayer->GetControllerId() >= 0)
//             {
//                 chessFriends->UpdateFriends(localPlayer->GetControllerId());
//             }
//             chessFriendUpdateTimer = 4;
        }
    }

    if (bGameEndedFrame)
    {
        bGameEndedFrame = false;

        // Only put code here which you don't want to be done when the game end is due to host loss
        
        // Do we want to show the match scoreboard?
        if (IsPrimaryPlayer())
        {
            // TODO: Implement after we have created the AChessHUD class
//             AChessHUD* chessHUD = GetChessHUD();
//             if (chessHUD)
//             {
//                 chessHUD->ShowScoreboard(true, true);
//             }
        }
    }
}

void AChessPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    // UI Input
    InputComponent->BindAction("InGameMenu", IE_Pressed, this, &AChessPlayerController::OnToggleInGameMenu);
    //InputComponent->BindAction("ToggleScoreboard", IE_Pressed, this, &AChessPlayerController::OnToggleScoreboard);
    
    // Chat Input
    InputComponent->BindAction("ToggleChat", IE_Pressed, this, &AChessPlayerController::ToggleChatWindow);

    // Voice Chat
    InputComponent->BindAction("PushToTalk", IE_Pressed, this, &APlayerController::StartTalking);
    InputComponent->BindAction("PushToTalk", IE_Pressed, this, &APlayerController::StopTalking);
}

void AChessPlayerController::GameHasEnded(class AActor* EndGameFocus /* = NULL */, bool bIsWinner /* = false */)
{
    UpdateSaveFileOnGameEnd(bIsWinner);
    // Leaderboards
    // Achievements

    Super::GameHasEnded(EndGameFocus, bIsWinner);
}

void AChessPlayerController::ClientReturnToMainMenu_Implementation(const FString& ReturnReason)
{
    UChessGameInstance* gameInstance = GetWorld() != NULL ? Cast<UChessGameInstance>(GetWorld()->GetGameInstance()) : NULL;

    if (!ensure(gameInstance != NULL))
    {
        return;
    }

    if (GetNetMode() == NM_Client)
    {
//         const FText returnReason = NSLOCTEXT("NetworkErrors", "HostQuit", "The host has quit the match");
//         const FText okayButton = NSLOCTEXT("DialogButtons", "OKAY", "OK");
// 
//         gameInstance->ShowMessageThenGotoState(returnReason, FText::GetEmpty(), ChessGameInstanceState::MainMenu);

        gameInstance->GotoState(ChessGameInstanceState::MainMenu);
    }
    else
    {
        gameInstance->GotoState(ChessGameInstanceState::MainMenu);
    }

    // Clear the flag so we don't do normal end of game stuff.
    bGameEndedFrame = false;
}

void AChessPlayerController::UpdateSaveFileOnGameEnd(bool isWinner)
{
    AChessPlayerState* chessPlayerState = Cast<AChessPlayerState>(PlayerState);
    if (chessPlayerState)
    {
        // Update local saved profile
        UChessPersistentUser* const persistentUser = GetPersistentUser();
        if (persistentUser)
        {
            persistentUser->AddMatchResult(isWinner);
            persistentUser->SaveIfDirty();
        }
    }
}