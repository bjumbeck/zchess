// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Online.h"
#include "GameFramework/PlayerController.h"
#include "ChessPlayerController.generated.h"

UCLASS()
class ZCHESS_API AChessPlayerController : public APlayerController
{
    GENERATED_BODY()

    public:
        AChessPlayerController(const FObjectInitializer& objectInitializer);

        #pragma region Documentation
        /// Sets the spectator location and rotation
        ///       
        /// @param cameraLocation       The camera location.
        /// @param cameraRotation       The camera rotation.
        #pragma endregion
        UFUNCTION(Reliable, Client)
        void ClientSetSpectatorCamera(FVector cameraLocation, FRotator cameraRotation);

        /// Notify player about the started match
        UFUNCTION(Reliable, Client)
        void ClientGameStarted();

        /// Starts the online game using the session name in the PlayerState
        UFUNCTION(Reliable, Client)
        void ClientStartOnlineGame();

        /// Ends the online game using the session name in the PlayerState
        UFUNCTION(Reliable, Client)
        void ClientEndOnlineGame();

        UFUNCTION(Reliable, Client)
        void ClientSendRoundEndEvent(bool isWinner, int32 expendedTimeInSeconds);

        #pragma region Documentation
        /// Notify the player about the finished match
        ///
        /// @param endGameFocus         The actor to focus at the end of the game
        /// @param isWinner             True if this player is the winner
        #pragma endregion
        virtual void ClientGameEnded_Implementation(class AActor* endGameFocus, bool isWinner);

        #pragma region Documentation
        /// Overridden message implementation
        ///
        /// @param senderPlayerState        If non-null, state of the sender player
        /// @param str                      The string
        /// @param type                     The type
        /// @param msgLifeTime              The message life time
        #pragma endregion
        virtual void ClientTeamMessage_Implementation(APlayerState* senderPlayerState, const FString& str, FName type, float msgLifeTime) override;

        /// Tell the HUD to toggle the chat window
        void ToggleChatWindow();

        /// Local function to say a string
        UFUNCTION(Exec)
        virtual void Say(const FString& msg);

        /// RPC for clients to talk to the server
        UFUNCTION(Unreliable, Server, WithValidation)
        void ServerSay(const FString& msg);

        /// Toggle InGameMenu handler
        void OnToggleInGameMenu();

        /// Returns a pointer to the chess game hud. Can be NULL
        class AChessHUD* GetChessHUD() const;

        /// Show the in-game menu if it's not already showing.
        void ShowInGameMenu();

        /// Check if game play related actions (Piece movement, etc) are allowed right now
        bool IsGameInputAllowed() const;

        /// Is game menu currently active?
        bool IsGameMenuVisible() const;

        /// Ends and/or destroys game session
        void CleanupSessionOnReturnToMenu();

        /// Initialize the input system from the player settings
        virtual void InitInputSystem() override;

        #pragma region Documentation
        /// Used to pause the game
        ///
        /// @param bPause               True to pause
        /// @param CanUnpauseDelegate   The can unpause delegate
        ///
        /// @return true if it succeeds, false if it fails
        #pragma endregion
        virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;

        /// Returns the persistent user record associated with this player, or null if there isn't one
        class UChessPersistentUser* GetPersistentUser() const;

        /// Cleans up any resources necessary to return to the main menu. Does not modify GameInstance state
        virtual void HandleReturnToMainMenu();

        #pragma region Documentation
        /// Associate a new UPlayer with this PlayerController
        ///
        /// @param player       If non-null, the new player
        #pragma endregion
        virtual void SetPlayer(UPlayer* player);

        virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;

    protected:
        /// Ran after all game elements are created
        virtual void PostInitializeComponents() override;

        /// Ran at the beginning of play
        virtual void BeginPlay() override;

        #pragma region Documentation
        /// Actors tick function
        ///
        /// @param DeltaTime                    The delta time
        /// @param TickType                     Type of the tick
        /// @param ThisTickFunction             The tick function
        #pragma endregion
        virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;

        /// Sets up the input component.
        virtual void SetupInputComponent() override;

        #pragma region Documentation
        /// Called from game info upon end of the game, used to transition to the proper state
        ///
        /// @param EndGameFocus         (Optional) If non-null, the Actor to focus on end game
        /// @param bIsWinner            True if this player is winner
        #pragma endregion
        virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;

        #pragma region Documentation
        /// Return the client to the main menu gracefully. ONLY sets GameInstace state
        ///
        /// @param ReturnReason         The reason for return
        #pragma endregion
        void ClientReturnToMainMenu_Implementation(const FString& ReturnReason) override;

        #pragma region Documentation
        /// Updates the player save file on game end
        ///
        /// @param isWinner             True if this player is the winner.
        #pragma endregion
        void UpdateSaveFileOnGameEnd(bool isWinner);

    protected:
        /// If set, game play related actions (Piece movement, etc.) are allowed
        uint8 bAllowGameActions : 1;
        /// True for the first frame after the game has ended
        uint8 bGameEndedFrame : 1;

        FName serverSayString;
        /// TODO: Do we need?
        float chessFriendUpdateTimer;
        /// For tracking whether or not to send the end event
        bool hasSentStartEvents;

        TSharedPtr<class FChessInGameMenu> chessInGameMenu;

    private:
        /// Handle for efficient management of ClientStartOnlineGame timer
        FTimerHandle timerHandle_ClientStartOnlineGame;
};
