// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "ChessPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ZCHESS_API AChessPlayerState : public APlayerState
{
    GENERATED_BODY()

    public:
        AChessPlayerState(const FObjectInitializer& objectInitializer); 
        virtual void Reset() override;
        FString GetShortPlayerName() const;

        FORCEINLINE void SetIsQuiter(bool value) { isQuiter = value; }
        FORCEINLINE bool IsQuiter() const { return isQuiter; }

    private:
        UPROPERTY()
        uint8 isQuiter : 1;
};
