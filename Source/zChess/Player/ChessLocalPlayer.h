// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/LocalPlayer.h"
#include "ChessLocalPlayer.generated.h"

/**
 * 
 */
UCLASS(config=Engine, Transient)
class ZCHESS_API UChessLocalPlayer : public ULocalPlayer
{
    GENERATED_BODY()

    public:
        UChessLocalPlayer(const FObjectInitializer& objectInitializer);

        #pragma region Documentation
        /// Sets controller identifier for this player
        ///
        /// @param NewControllerId      New ID for the controller
        #pragma endregion
        virtual void SetControllerId(int32 NewControllerId) override;

        /// Gets the nickname for this player
        virtual FString GetNickname() const;

        /// Gets the persistent user data for this player
        class UChessPersistentUser* GetPersistentUser() const;

        /// Loads persistent user data for this player
        void LoadPersistentUser();

    private:
        #pragma region Documentation
        /// Gets online session class to use for this player
        ///
        /// @return The online session class for this player
        #pragma endregion
        TSubclassOf<UOnlineSession> GetOnlineSessionClass() override;

    private:
        /// The persistent user save data
        class UChessPersistentUser* persistentUser;
};
