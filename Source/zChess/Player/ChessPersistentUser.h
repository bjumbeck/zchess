// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/SaveGame.h"
#include "ChessPersistentUser.generated.h"

/**
 * 
 */
UCLASS()
class ZCHESS_API UChessPersistentUser : public USaveGame
{
    GENERATED_BODY()

    public:
        UChessPersistentUser(const FObjectInitializer& ObjectInitializer);  

        #pragma region Documentation
        /// Loads persistent user data if it exists, creates and empty record otherwise
        ///
        /// @param slotName     Name of the slot.
        /// @param userIndex    Zero-based index of the user.
        ///
        /// @return null if it fails, else the persistent user.
        #pragma endregion
        static UChessPersistentUser* LoadPersistentUser(FString slotName, const int32 userIndex);

        /// Saves data if anything has changed
        void SaveIfDirty();

        #pragma region Documentation
        /// Records the result of a match
        ///
        /// @param isWinner     True if the player was the winner
        #pragma endregion
        void AddMatchResult(bool isWinner);

        FORCEINLINE int32 GetUserIndex() const { return userIndex; }
        FORCEINLINE int32 GetWins() const { return wins; }
        FORCEINLINE int32 GetLosses() const { return losses; }
        FORCEINLINE FString GetName() const { return slotName; }

    protected:
        /// Saves the persistent user data
        void SavePersistentUser();

    protected:
        UPROPERTY()
        int32 wins;
        UPROPERTY()
        int32 losses;

    private:
        bool isDirty;
        
        int32 userIndex;
        FString slotName;
};
