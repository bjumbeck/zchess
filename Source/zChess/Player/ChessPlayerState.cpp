// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPlayerState.h"

AChessPlayerState::AChessPlayerState(const FObjectInitializer& objectInitializer)
    : Super(objectInitializer)
{
    isQuiter = false;
}

void AChessPlayerState::Reset()
{
    Super::Reset();

    // Player states persist across seamless travel, so we need to reset some stats before a new match can be played
    isQuiter = false;
}

FString AChessPlayerState::GetShortPlayerName() const
{
    if (PlayerName.Len() > MAX_PLAYER_NAME_LENGTH)
    {
        return PlayerName.Left(MAX_PLAYER_NAME_LENGTH) + "...";
    }

    return PlayerName;
}