// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessLocalPlayer.h"
#include "ChessPersistentUser.h"
#include "Online.h"
#include "OnlineSessionClient.h"

UChessLocalPlayer::UChessLocalPlayer(const FObjectInitializer& objectInitializer)
    : Super(objectInitializer)
{   
}

void UChessLocalPlayer::SetControllerId(int32 NewControllerId)
{
    ULocalPlayer::SetControllerId(NewControllerId);

    // If we changed controller id / user, then we need to load the correct persistent user.
    if (persistentUser != nullptr && (GetControllerId() != persistentUser->GetUserIndex() || GetNickname() != persistentUser->GetName()))
    {
        persistentUser->SaveIfDirty();
        persistentUser = nullptr;
    }

    if (!persistentUser)
    {
        LoadPersistentUser();
    }
}

FString UChessLocalPlayer::GetNickname() const
{
    FString userNickName = Super::GetNickname();

    if (userNickName.Len() > MAX_PLAYER_NAME_LENGTH)
    {
        userNickName = userNickName.Left(MAX_PLAYER_NAME_LENGTH) + "...";
    }

    // Check for duplicate names... and prevent reentry.
    bool replace = userNickName.Len() == 0;
    static bool reentry = false;
    if (!reentry)
    {
        reentry = true;
        UGameInstance* gameInstance = GetWorld() != NULL ? GetWorld()->GetGameInstance() : NULL;
        if (gameInstance)
        {
            // Check all names that occur before ours that are the same
            const TArray<ULocalPlayer*>& localPlayers = gameInstance->GetLocalPlayers();
            for (int i = 0; i < localPlayers.Num(); ++i)
            {
                const ULocalPlayer* localPlayer = localPlayers[i];
                if (this == localPlayer)
                {
                    break;
                }

                if (userNickName == localPlayer->GetNickname())
                {
                    replace = true;
                    break;
                }
            }
        }

        reentry = false;
    }

    if (replace)
    {
        userNickName = FString::Printf(TEXT("Player%i"), GetControllerId() + 1);
    }

    return userNickName;
}

UChessPersistentUser* UChessLocalPlayer::GetPersistentUser() const
{
    // If not loaded yet load it.
    if (persistentUser == nullptr)
    {
        UChessLocalPlayer* const mutableThis = const_cast<UChessLocalPlayer*>(this);
        mutableThis->LoadPersistentUser();
    }

    return persistentUser;
}

void UChessLocalPlayer::LoadPersistentUser()
{
    // if we changed controller id / user, then we need to load the appropriate persistent user.
    if (persistentUser != nullptr && (GetControllerId() != persistentUser->GetUserIndex() || GetNickname() != persistentUser->GetName()))
    {
        persistentUser->SaveIfDirty();
        persistentUser = nullptr;
    }

    if (persistentUser == nullptr)
    {
        FPlatformUserId platformId = GetControllerId();

        auto identity = Online::GetIdentityInterface();
        if (identity.IsValid() && GetPreferredUniqueNetId().IsValid())
        {
            platformId = identity->GetPlatformUserIdFromUniqueNetId(*GetPreferredUniqueNetId());
        }

        persistentUser = UChessPersistentUser::LoadPersistentUser(GetNickname(), platformId);
    }
}

TSubclassOf<UOnlineSession> UChessLocalPlayer::GetOnlineSessionClass()
{
    return UOnlineSessionClient::StaticClass();
}