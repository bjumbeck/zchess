// Fill out your copyright notice in the Description page of Project Settings.

#include "zChess.h"
#include "ChessPersistentUser.h"

UChessPersistentUser::UChessPersistentUser(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)  
{
    isDirty = false;
}

UChessPersistentUser* UChessPersistentUser::LoadPersistentUser(FString slotName, const int32 userIndex)
{
    UChessPersistentUser* result = nullptr;

    // first set of player signins can happen before the UWorld exists, which means no OSS, which means no user names, which means no slotnames.
    // Persistent users aren't valid in this state.
    if (slotName.Len() > 0)
    {
        result = Cast<UChessPersistentUser>(UGameplayStatics::LoadGameFromSlot(slotName, userIndex));
        if (result == NULL)
        {
            // If failed to load create a new one
            result = Cast<UChessPersistentUser>(UGameplayStatics::CreateSaveGameObject(UChessPersistentUser::StaticClass()));
        }

        check(result != NULL);

        result->slotName = slotName;
        result->userIndex = userIndex;
    }

    return result;
}

void UChessPersistentUser::SaveIfDirty()
{
    if (isDirty)
    {
        SavePersistentUser();
    }
}

void UChessPersistentUser::AddMatchResult(bool isWinner)
{
    if (isWinner)
    {
        wins++;
    }
    else
    {
        losses++;
    }

    isDirty = true;
}

void UChessPersistentUser::SavePersistentUser()
{
    UGameplayStatics::SaveGameToSlot(this, slotName, userIndex);
    isDirty = false;
}
