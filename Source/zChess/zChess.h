// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "Net/UnrealNetwork.h"

DECLARE_LOG_CATEGORY_EXTERN(LogChess, Log, All);

// Global Defines
#define MAX_PLAYER_NAME_LENGTH 16